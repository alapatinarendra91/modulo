package chs.validateui;

 
public interface Validation {

    String getErrorMessage();

    boolean isValid(String text);

}