package chs.validateui;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class FormUtils {

    /**
     * Showing the keyboard
     * @param context
     * @param textView
     */
    public static void showKeyboard(Context context, TextView textView) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null) {
            // only will trigger it if no physical keyboard is open
            imm.showSoftInput(textView, 0);
        }
    }

    /**
     * Hideing the keyboard
     * @param context
     * @param textView
     */
    public static void hideKeyboard(Context context, View textView) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null) {
            // only will trigger it if no physical keyboard is open
            imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
        }
    }

    /**
     *
     * @param one
     * @param two
     * @return difference between 2 dates
     */
    public static long daysBetween(String one, String two) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date previousStrDate = null;
        Date currentStrDate = null;

        try {
            previousStrDate = sdf.parse(one);
            currentStrDate = sdf.parse(two);
            long difference = (previousStrDate.getTime()-currentStrDate.getTime())/86400000;
            return Math.abs(difference);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

//    /**
//     *
//     * @param days
//     * @return
//     */
//    public static int getYear(long days){
//        int year=(int)days/365;
//        return year;
//    }

    /**
     * Getting month from days
     * @param days
     * @return
     */
    public static int getMonth(long days){
        int month=(int)days/30;
//        Log.e("Month","Toatal month : "+month+" , actual month : "+month%12);
        if (month%12==0){
            return 0;
        }else {
            return month%12;
        }
    }

    /**
     *
     * @param previousDate
     * @param currentDate
     * @return
     */
    public static boolean DateComparator(String previousDate, String currentDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date previousStrDate = null;
        Date currentStrDate = null;
        try {
            previousStrDate = sdf.parse(previousDate);
            currentStrDate = sdf.parse(currentDate);
            // android.util.Log.e("daysBetween","daysBetween: "+daysBetween(previousStrDate,currentStrDate));
            if (currentStrDate.after(previousStrDate)) {
                return true;
            }else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

}