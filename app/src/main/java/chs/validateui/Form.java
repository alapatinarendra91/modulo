package chs.validateui;

import android.app.Activity;
import android.text.Html;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;


public class Form {

    private List<Field> mFields = new ArrayList<Field>();
    private Activity mActivity;

    public Form(Activity activity) {
        this.mActivity = activity;
    }

    public void addField(Field field) {
        mFields.add(field);
    }

    public boolean isValid() {
        boolean result = true;
        try {
            for (Field field : mFields) {
                result &= field.isValid();
            }
        } catch (FieldValidationException e) {
            result = false;

            EditText textView = e.getTextView();
            textView.requestFocus();
            textView.selectAll();

            FormUtils.showKeyboard(mActivity, textView);

            showErrorMessage(e.getMessage(),textView);
        }
        return result;
    }

    protected void showErrorMessage(String message, EditText textView) {
    	textView.setError(Html.fromHtml("<font color='red'>" + message + "</font>"));

    }


}