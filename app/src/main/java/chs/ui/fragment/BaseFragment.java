/*
 *
 *  Proprietary and confidential. Property of Calibrage info systems. Do not disclose or distribute.
 *  You must have written permission from Calibrage info systems. to use this code.
 *
 */

package chs.ui.fragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import chs.ui.IScreen;
import chs.ui.activity.BaseActivity;


/**
 * @author Narendra
 */
public abstract class BaseFragment extends Fragment implements IScreen {

    /**
     * @return
     */
    protected BaseActivity getBaseActivity() {
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity) || activity.isFinishing()) {
            return null;
        }
        return (BaseActivity) activity;
    }

 }
