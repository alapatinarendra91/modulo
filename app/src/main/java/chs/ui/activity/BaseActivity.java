/*
 *
 *  Proprietary and confidential. Property of Calibrage. Do not disclose or distribute.
 *  You must have written permission from Calibrage. to use this code.
 *
 */

package chs.ui.activity;

import android.app.Application;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.multidex.BuildConfig;

import com.chs.modulo.R;
import com.chs.modulo.Utils.CommonUtils;

import chs.application.BaseApplication;
import chs.ext.RequestManager;
import chs.ui.IScreen;
import chs.utils.KeypadUtils;
import chs.utils.ProgressHUD;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * This class is used as base-class for application-base-activity.
 */
public abstract class BaseActivity extends AppCompatActivity implements IScreen {

    private String LOG_TAG = getClass().getSimpleName();
    public ProgressHUD mProgressDialog;
    public String ACTION_DEATH_REGISTER_COMPLETE = "action_death_register_complete";
    private Toolbar toolbar;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext((newBase));
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getContentViewId() != 0)
            setContentView(getContentViewId());

        /*Registering Local broad cast receiver*/
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(ACTION_DEATH_REGISTER_COMPLETE));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

    }

    /**
     * Set the tool bar Title
     *
     * @param title
     *
     */
    public void setToolBarHeading(final String title) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (toolbar != null) {
            toolbar.setTitle("");
            mTitle.setText("" + title);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        }
    }

    /**
     * Set the tool bar Title with back arrow enabled/disabled
     *
     * @param title
     * @param isDisplayHomeAsUpEnabled  (Back arrow enabled/disabled)
     *
     */
    public void setToolBarHeading(final String title, boolean isDisplayHomeAsUpEnabled) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (toolbar != null) {
            toolbar.setTitle("");
            mTitle.setText("" + title);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(isDisplayHomeAsUpEnabled);
        }
    }


    /**
     * Set the tool bar Title
     *
     * @param title
     * @param subTitle
     * @param isDisplayHomeAsUpEnabled
     */
    public void setToolBarHeading(String title, final String subTitle, boolean isDisplayHomeAsUpEnabled) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (toolbar != null) {
            toolbar.setTitle("");
            mTitle.setText("" + title);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(isDisplayHomeAsUpEnabled);

            toolbar.post(new Runnable() {
                @Override
                public void run() {
                    toolbar.setSubtitle("" + subTitle);
                }
            });
        }
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();

        }
    };

    protected abstract int getContentViewId();

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_TAG, "onResume()");

        Application application = this.getApplication();
        if (application instanceof BaseApplication) {
            BaseApplication baseApplication = (BaseApplication) application;
            if (baseApplication.isAppInBackground()) {
                onAppResumeFromBackground();

            }
            baseApplication.onActivityResumed();
        }
    }

    /**
     * This callback will be called after onResume if application is being
     * resumed from background. <br/>
     * <p/>
     * Subclasses can override this method to get this callback.
     */
    protected void onAppResumeFromBackground() {
        if (BuildConfig.DEBUG) {
            Log.i(LOG_TAG, "onAppResumeFromBackground()");
        }

    }

    /**
     * This method should be called to force app assume itself not in
     * background.
     */
    public final void setAppNotInBackground() {
        Application application = this.getApplication();
        if (application instanceof BaseApplication) {
            BaseApplication baseApplication = (BaseApplication) application;
            baseApplication.setAppInBackground(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) {
            Log.i(LOG_TAG, "onPause()");
        }

        Application application = this.getApplication();
        if (application instanceof BaseApplication) {
            BaseApplication baseApplication = (BaseApplication) application;
            baseApplication.onActivityPaused();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (BuildConfig.DEBUG) {
            Log.i(LOG_TAG, "onNewIntent()");
        }
    }

    // ////////////////////////////// show and hide ProgressDialog

    /**
     * Subclass should over-ride this method to update the UI with response,
     * this base class promises to call this method from UI thread.
     *
     * @param serviceResponse
     */
    public abstract void updateUi(final boolean status, final int action, final Object serviceResponse);

    /**
     * Shows a simple native progress dialog<br/>
     * Subclass can override below two methods for custom dialogs- <br/>
     * 1. showProgressDialog <br/>
     * 2. removeProgressDialog
     *
     * @param bodyText
     */

    public void showProgressDialog(String bodyText, final boolean isRequestCancelable) {
        try {
            if (isFinishing()) {
                return;
            }
            if (mProgressDialog == null) {
                mProgressDialog = ProgressHUD.show(this, "", isRequestCancelable, false, null);
                mProgressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_CAMERA || keyCode == KeyEvent.KEYCODE_SEARCH) {
                            return true; //
                        } else if (keyCode == KeyEvent.KEYCODE_BACK && isRequestCancelable) {
                            CommonUtils.logPrint("Ondailogback", "cancel dailog");
                            RequestManager.cancelRequest();
                            dialog.dismiss();
                            return true;
                        }
                        return false;
                    }
                });
            }

            // mProgressDialog.setMessage(bodyText);

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (Exception e) {

        }
    }


    /**
     * Removes the simple native progress dialog shown via showProgressDialog <br/>
     * Subclass can override below two methods for custom dialogs- <br/>
     * 1. showProgressDialog <br/>
     * 2. removeProgressDialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {

        }
    }

    public void setProgressDialog(ProgressHUD dialog) {
        this.mProgressDialog = dialog;
    }

    // ////////////////////////////// show and hide key-board
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                KeypadUtils.hideSoftKeypad(this);
            }
        }
        return ret;
    }



}