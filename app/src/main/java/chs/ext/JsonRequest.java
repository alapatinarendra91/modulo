/*
 *
 *  Proprietary and confidential. Property of Calibrage info systems. Do not disclose or distribute.
 *  You must have written permission from Calibrage info systems. to use this code.
 *
 */

package chs.ext;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyLog;
import com.chs.modulo.Utils.CommonUtils;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Map;

/**
 * A request for retrieving a {@link JSONObject} response body at a given URL, allowing for an
 * optional {@link JSONObject} to be passed in as part of the request body.
 */
public abstract class JsonRequest<T> extends Request<T> {

    String TAG="JsonRequest";
    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";

    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    private final String mRequestBody;

    /**
     * Request headers.
     */
    protected Map<String, String> mRequestHeaders;
    protected NetworkResponse mResponse;
    private Priority mPriority;

    public JsonRequest(String url, String jsonPayload, ErrorListener errorListener) {
        this(url, Collections.<String, String>emptyMap(), jsonPayload, errorListener);
    }

    public JsonRequest(String url, Map<String, String> mRequestHeaders, String jsonPayload, ErrorListener errorListener) {
        this(jsonPayload == null ? Method.GET : jsonPayload.toLowerCase().contains("patchmethod") ? Method.PATCH : Method.POST, url, mRequestHeaders, jsonPayload, errorListener);

    }

    public JsonRequest(int method, String url, Map<String, String> mRequestHeaders, String jsonPayload, ErrorListener errorListener) {
        super(method, url, errorListener);
        CommonUtils.logPrint("Method is: " + method);
        this.mRequestBody = jsonPayload;
        this.mRequestHeaders = mRequestHeaders;

        //Log.e("Requestheader",mRequestHeaders.toString());
        //Log.e("RequestBody",mRequestBody);

        CommonUtils.logPrint(TAG,"URL : "+ url);

        if (mRequestBody != null) {
           // Log.e("RequestBody", mRequestBody);
        } else{
            CommonUtils.logPrint("RequestBody", " mRequestBody is NULL");
        }
    }

    @Override
    public Priority getPriority() {
        return this.mPriority;
    }

    public void setPriority(Priority mPriority) {
        this.mPriority = mPriority;
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {

            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        return mRequestHeaders;
    }


}
