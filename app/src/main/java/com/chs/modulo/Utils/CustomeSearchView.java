package com.chs.modulo.Utils;

import android.app.Dialog;
import android.content.Context;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;

import com.chs.modulo.R;

import java.lang.reflect.Field;

/**
 * Created by CIS16 on 28-Dec-17.
 */


public class CustomeSearchView {

    private static OnSearchListener onSearchListener;
    private static Context mContext;
    String mSearchHint = "";

    public void loadToolBarSearch(Context context, String searchHint) {
        mContext = context;
        mSearchHint = searchHint;
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_toolbar_search, null);
        ImageView imgToolBack = (ImageView) view.findViewById(R.id.img_tool_back);
        final SearchView edtToolSearch = (SearchView) view.findViewById(R.id.edt_tool_search);
        LinearLayout mainLay = (LinearLayout) view.findViewById(R.id.mainLay);


        edtToolSearch.onActionViewExpanded();
        /*Changing the cursor color*/
        changeCursorColor(edtToolSearch);

        final Dialog toolbarSearchDialog = new Dialog(mContext, R.style.AppTheme);
        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.TOP);
//        toolbarSearchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();
        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        edtToolSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (edtToolSearch.getQuery().length() >= 3) {
                    onSearchListener.setSearchListener("search", edtToolSearch.getQuery().toString());
                    toolbarSearchDialog.dismiss();
                    edtToolSearch.clearFocus();
                    edtToolSearch.onActionViewCollapsed();
                } else
                    CommonUtils.showToast(mContext,"Please enter minimum 3 characters");

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }

        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarSearchDialog.dismiss();
//                searchViewItem.collapseActionView();
            }
        });

    }

    /**
     * @param searchViewAndroidActionBar
     */
    private void changeCursorColor(SearchView searchViewAndroidActionBar) {

        final AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchViewAndroidActionBar.findViewById(R.id.search_src_text);
        try {
            searchTextView.setHint(mSearchHint);
            searchTextView.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setOnSearchListener(OnSearchListener onSearchListener) {
        this.onSearchListener = onSearchListener;
    }

    public interface OnSearchListener {
        void setSearchListener(String status, String searchString);
    }
}
