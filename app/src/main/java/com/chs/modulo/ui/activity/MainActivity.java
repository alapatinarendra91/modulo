package com.chs.modulo.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.chs.modulo.R;
import com.chs.modulo.Utils.CommonUtils;
import com.chs.modulo.Utils.PrefUtil;
import com.chs.modulo.adapter.ExpandableListAdapter;
import com.chs.modulo.constants.AppConstants;
import com.chs.modulo.constants.ErrorModel;
import com.chs.modulo.constants.Event;
import com.chs.modulo.constants.VolleyErrorListener;
import com.chs.modulo.interfaces.WebServices;
import com.chs.modulo.model.MenuModel;
import com.chs.modulo.model.ResponceModel;
import com.chs.modulo.ui.fragment.AddressFragment;
import com.chs.modulo.ui.fragment.ChangePasswordFragment;
import com.chs.modulo.ui.fragment.ChangeProfileImageFragment;
import com.chs.modulo.ui.fragment.CustomersFragment;
import com.chs.modulo.ui.fragment.DesignFragment;
import com.chs.modulo.ui.fragment.HomeFragment;
import com.chs.modulo.ui.fragment.ItemsFragment;
import com.chs.modulo.ui.fragment.OrdersListFragment;
import com.chs.modulo.ui.fragment.PaymentsFragment;
import com.chs.modulo.ui.fragment.UploadImage;
import com.chs.modulo.ui.fragment.UsersFragment;
import com.google.android.material.navigation.NavigationView;
import com.warehouse.ui.activity.LoginActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import chs.ext.GsonObjectRequest;
import chs.ext.RequestManager;

import static com.chs.modulo.constants.AppConstants.Intent_ComingFrom;

public class MainActivity extends ModuloBaseActivity
{
    private Context mContext;
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    private TextView userNameTxt, userEmailTxt;
    private ImageView userImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = this;

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        setToolBarHeading("Home");
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        userImage = header.findViewById(R.id.userImage);
        userNameTxt = header.findViewById(R.id.userNameTxt);
        userEmailTxt = header.findViewById(R.id.userEmailTxt);

        userNameTxt.setText("" + PrefUtil.getString(mContext, AppConstants.PREF_DisplayName, AppConstants.PREF_NAME) + "\n"
                + PrefUtil.getString(mContext, AppConstants.PREF_MobileNo, AppConstants.PREF_NAME));

        userEmailTxt.setText("" + PrefUtil.getString(mContext, AppConstants.PREF_Email, AppConstants.PREF_NAME));

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, new UploadImage()).commit();
//                onBackPressed();
            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
//        getSupportFragmentManager().beginTransaction().replace(R.id.container, new PaymentsFragment()).addToBackStack("back").commit();

        if (getIntent() != null){
            boolean gotoChangePswdScreen = getIntent().getBooleanExtra(AppConstants.Intent_resetPasswordReqd, false);
            if (gotoChangePswdScreen)
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new ChangePasswordFragment()).commit();
        }



        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            CommonUtils.requestPermission(mContext, Event.PERMISSION_REQUEST_File);
        } else if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            CommonUtils.requestPermission(mContext, Event.PERMISSION_REQUEST_Camera);

        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            CommonUtils.requestPermission(mContext, Event.PERMISSION_REQUEST_Location);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Callback for the result from requesting permissions. This method is invoked for every call on requestPermissions(String[], int).
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Event.PERMISSION_REQUEST_Camera:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, you cannot access this feature", Toast.LENGTH_LONG).show();
                }
                break;

            case Event.PERMISSION_REQUEST_File:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CommonUtils.requestPermission(mContext, Event.PERMISSION_REQUEST_Camera);
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, you cannot access this feature", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }


    private void prepareMenuData() {


        MenuModel menuModel = new MenuModel("Home", true, false);
        headerList.add(menuModel);

        menuModel = new MenuModel("My Profile", true, true);
        headerList.add(menuModel);

        List<MenuModel> childModelsList = new ArrayList<>();

        childModelsList.add(new MenuModel("Change Password", false, false));
//        childModelsList.add(new MenuModel("Upload photo", false, false));
        childModelsList.add(new MenuModel("Change Address", false, false));

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Master info", true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();
        childModelsList.add(new MenuModel("Items", false, false));
        childModelsList.add(new MenuModel("Designs", false, false));
        childModelsList.add(new MenuModel("Shades", false, false));
        childModelsList.add(new MenuModel("Kinds", false, false));
        childModelsList.add(new MenuModel("Sizes", false, false));
        childModelsList.add(new MenuModel("Orders", false, false));
        childModelsList.add(new MenuModel("Payments", false, false));
        childModelsList.add(new MenuModel("Customers", false, false));

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("App Configurations", true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();
        childModelsList.add(new MenuModel("Users", false, false));
        childModelsList.add(new MenuModel("Roles", false, false));
        childModelsList.add(new MenuModel("Applications", false, false));
        childModelsList.add(new MenuModel("Channels", false, false));
        childModelsList.add(new MenuModel("Code Masters", false, false));
        childModelsList.add(new MenuModel("System Parameters", false, false));
        childModelsList.add(new MenuModel("Services", false, false));

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        menuModel = new MenuModel("Language Management", true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();
        childModelsList.add(new MenuModel("Lang Keys", false, false));
        childModelsList.add(new MenuModel("Lang Translations", false, false));

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        menuModel = new MenuModel("Monitoring", true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();
        childModelsList.add(new MenuModel("Session Audit", false, false));
        childModelsList.add(new MenuModel("Transaction Audit", false, false));
        childModelsList.add(new MenuModel("Service Log", false, false));

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Reporting", true, true);
        headerList.add(menuModel);

        childModelsList = new ArrayList<>();
        childModelsList.add(new MenuModel("Orders Summary", false, false));
        childModelsList.add(new MenuModel("Payments Summary", false, false));

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }


        headerList.add(new MenuModel("Logout", true, false));

    }
    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if(groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        switch (groupPosition){
                            case 0:
                                getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                                break;
                            case 3:
                                getSupportFragmentManager().beginTransaction().replace(R.id.container, new OrdersListFragment()).commit();
                                break;
                            case 4:
                                getSupportFragmentManager().beginTransaction().replace(R.id.container, new CustomersFragment()).commit();
                                break;
                            case 5:
                                getSupportFragmentManager().beginTransaction().replace(R.id.container, new UsersFragment()).commit();
                                break;
                            case 6:

                                break;
                            case 7:
                                getData(Event.logout);
//                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                break;
                        }
                        onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {

                    int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                    parent.setItemChecked(index, true);

                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                        onBackPressed();
                        switch (groupPosition){
                            case 1:
                                if (childPosition == 0){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new ChangePasswordFragment()).addToBackStack("back").commit();
                                }
                                if (childPosition == 1){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new AddressFragment()).addToBackStack("back").commit();
                                }
                                break;

                            case 2:
                                if (childPosition == 0){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new ItemsFragment().newInstance(getString(R.string.items), "")).addToBackStack("back").commit();
                                }
                                if (childPosition == 1){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new DesignFragment().newInstance(getString(R.string.design), "")).addToBackStack("back").commit();
                                }
                                if (childPosition == 2){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new DesignFragment().newInstance(getString(R.string.shades), "")).addToBackStack("back").commit();
                                }
                                if (childPosition == 3){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new DesignFragment().newInstance(getString(R.string.kinds), "")).addToBackStack("back").commit();
                                }
                                if (childPosition == 4){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new DesignFragment().newInstance(getString(R.string.sizes), "")).addToBackStack("back").commit();
                                }
                                if (childPosition == 5){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new OrdersListFragment()).addToBackStack("back").commit();
                                }
                                if (childPosition == 6){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new PaymentsFragment()).addToBackStack("back").commit();
                                }
                                if (childPosition == 7){
                                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new CustomersFragment()).addToBackStack("back").commit();
                                }
                                break;

                        }

                }

                return false;
            }
        });
    }

    /**
     * Requesting api calls
     * */
    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<String, String>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        reqHeader.put("Authorization", (PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)));
        String request = "";

        switch (actionID) {
            case Event.logout:
                RequestManager.addRequest(new GsonObjectRequest<ResponceModel>
                        (WebServices.logout, reqHeader, request, ResponceModel.class, new VolleyErrorListener(this, Event.logout)) {
                    @Override
                    protected void deliverResponse(ResponceModel response) {
                        updateUi(true, Event.logout, response);
                    }
                });
                break;

        }
    }

    /*Getting responce from Api's and bind to model class*/
    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.logout:
//                    ResponceModel mVersionModel = (ResponceModel) serviceResponse;
                    PrefUtil.clearAllSharedPrefarences(mContext, AppConstants.PREF_NAME);
                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();

                    break;
            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "Logout failed");
        }

    }

}

