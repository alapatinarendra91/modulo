package com.chs.modulo.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.DashBoardObjectModel
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : ModuloBaseFragment(){
    private var rootView: View? = null
    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_home, null, false)
        mContext = activity
        setToolBarHeading("Home")
        getData(Event.adminBulletIn)
        return rootView
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.adminBulletIn -> {
                RequestManager.addRequest(object : GsonObjectRequest<DashBoardObjectModel>(
                    WebServices.adminBulletIn,
                    reqHeader, null, DashBoardObjectModel::class.java, VolleyErrorListener(this, Event.adminBulletIn)
                ) {
                    override fun deliverResponse(response: DashBoardObjectModel) {
                        updateUi(true, Event.adminBulletIn, response)
                    }
                })
            }
        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.adminBulletIn -> {
                    var madminBulletInModel = serviceResponse as DashBoardObjectModel
//                    totalItems.setText("Total Items: " + madminBulletInModel.data.it)
                    activeOrders.setText("Active orders: " + madminBulletInModel.data.activeOrders)
                    totalExcitives.setText("Total executives: " + madminBulletInModel.data.totalAgents)
                    totalCusrtomers.setText("Total customers: " + madminBulletInModel.data.totalCustomers)
                    monthsBill.setText("12 Months bill: " + madminBulletInModel.data.paymentReceipts12Months)
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, "Failed to get dash board data")
        }
    }

}