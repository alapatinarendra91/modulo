package com.chs.modulo.ui.fragment;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.chs.modulo.R;
import com.chs.modulo.ui.activity.ModuloBaseApplication;
import com.chs.modulo.ui.activity.ModuloBaseActivity;

import chs.ui.activity.BaseActivity;
import chs.ui.fragment.BaseFragment;

import static android.R.attr.id;

/**
 * @author albin
 * @date 23/6/15
 */
public class ModuloBaseFragment extends BaseFragment implements ModuloBaseActivity.OnBackHandle{

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    private Context mContext;

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {

    }

    @Override
    public void onEvent(int eventId, Object eventData) {

    }

    @Override
    public void getData(int actionID) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // instance = this;
    }


    /*Set the tool bar Title*/
    public void setToolBarHeading(String title) {
        if (getActivity() == null)
            return;
        ((BaseActivity) getActivity()).setToolBarHeading(title);
    }

  /*Set the tool bar Title and back button arrow*/
    public void setToolBarHeading(String title, boolean isDisplayHomeAsUpEnabled) {
        if (getActivity() == null)
            return;
        ((BaseActivity) getActivity()).setToolBarHeading(title, isDisplayHomeAsUpEnabled);
    }


    /**
     * Show progress window
     *
     * @param bodyText            body text
     * @param isRequestCancelable is request is cancel on back press
     */
    public void showProgressDialog(String bodyText, boolean isRequestCancelable) {
        if (getActivity() == null)
            return;
        ((BaseActivity) getActivity()).showProgressDialog(bodyText, isRequestCancelable);
    }

    /***
     * remove progress window
     */
    public void removeProgressDialog() {
        try {
            if ((BaseActivity) getActivity() != null) {
                ((BaseActivity) getActivity()).removeProgressDialog();
            }
        } catch (Exception e) {

        }
    }

    public void download() {
        mContext=getActivity();

        mNotifyManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setContentTitle("Picture Download")
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.ic_launcher_background);
// Start a lengthy operation in a background thread
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int incr;
                        // Do the "lengthy" operation 20 times
                        for (incr = 0; incr <= 100; incr += 5) {
                            // Sets the progress indicator to a max value, the
                            // current completion percentage, and "determinate"
                            // state
                            mBuilder.setProgress(100, incr, false);
                            // Displays the progress bar for the first time.
                            mNotifyManager.notify(id, mBuilder.build());
                            // Sleeps the thread, simulating an operation
                            // that takes time
                            try {
                                // Sleep for 5 seconds
                                Thread.sleep(5 * 1000);
                            } catch (InterruptedException e) {

                            }
                        }
                        // When the loop is finished, updates the notification
                        mBuilder.setContentText("Download complete")
                                // Removes the progress bar
                                .setProgress(0, 0, false);
                        mNotifyManager.notify(id, mBuilder.build());

                    }
                }
// Starts the thread by calling the run() method in its Runnable
        ).start();
    }

    @Override
    public boolean onBackPressed() {
        if (ichildBackPressed != null) {
            return ichildBackPressed.onChildBackPressed();
        } else {
            return false;
        }
    }

//    @Override
//    public abstract void updateTitle();

    @Override
    public void onResume() {
        // Always call the superclass method first
        super.onResume();

        ((ModuloBaseActivity)this.getActivity()).setGetAtBaseFragment(this);
    }

    /***
     * Replace a fragment
     * @param fragment new fragment
     * @param bundle bundle data
     * @param addToBackStack is fragment add in back stack?
     * @param anim is any animation?
     * @param retainInstance is fragment instance save?
     * @param sourceFragment current fragment
     * @param containerId //container id in xml
     */
    public void
    replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment sourceFragment, int containerId) {
        ((ModuloBaseActivity) this.getActivity()).replaceFragment(fragment, bundle, addToBackStack, anim, retainInstance, sourceFragment, containerId);
    }

    /***
     * Add fragment
     * @param fragment new fragment
     * @param bundle bundle data
     * @param addToBackStack is fragment add in back stack?
     * @param anim is any animation?
     * @param retainInstance is fragment instance save?
     * @param currentFragment current fragment
     * @param containerId //container id in xml
     */
    public void addFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment currentFragment, int containerId) {
        ((ModuloBaseActivity) this.getActivity()).addFragment(fragment, bundle, addToBackStack, anim, retainInstance, currentFragment, containerId);
    }

    /***
     * Add fragment for a specified tag
     * @param fragment new fragment
     * @param bundle bundle data
     * @param addToBackStack is fragment add in back stack?
     * @param anim is any animation?
     * @param retainInstance is fragment instance save?
     * @param currentFragment current fragment
     * @param containerId //container id in xml
     * @param tag tag string
     */
    public void addRecentFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, boolean anim, boolean retainInstance, Fragment currentFragment, int containerId, String tag) {
        ((ModuloBaseActivity) this.getActivity()).addRecentFragment(fragment, bundle, addToBackStack, anim, retainInstance, currentFragment, containerId, tag);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onMyActivityResult(int requestCode, int resultCode, Intent data) {

    }

    /***
     * Get Application context
     * @return Application instance
     */
    public ModuloBaseApplication getAtApplication(){
        if(getActivity()==null)
            return null;
        return ((ModuloBaseActivity) getActivity()).getAtApplication();
    }

    /***
     * Child fragment back press Listener
     */
    private IchildBackPressed ichildBackPressed;

    /**
     * Child callBack Listener register
     * @param ichildBackPressed
     */
    public void setIChildBAckPressed(IchildBackPressed ichildBackPressed) {
        this.ichildBackPressed = ichildBackPressed;
    }

    public interface IchildBackPressed {
        boolean onChildBackPressed();
    }

    /***
     * Remove a fragment from back stack
     */
    public void removeFragment() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(this);
        trans.commit();
        manager.popBackStack();
        manager.executePendingTransactions();
    }


}
