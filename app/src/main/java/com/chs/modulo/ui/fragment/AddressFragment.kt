package com.chs.modulo.ui.fragment

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.NotEmpty

import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_address.*
import kotlinx.android.synthetic.main.fragment_address.view.*
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AddressFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AddressFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddressFragment : ModuloBaseFragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var rootView: View? = null
    private var mContext: Context? = null
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    lateinit var addressModel: AddressDTO

    override fun onCreate(savedInstanceState: Bundle?) {super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        setToolBarHeading("Address")
        rootView =  inflater.inflate(R.layout.fragment_address, container, false)
        mContext = activity
        rootView!!.saveBtn.setOnClickListener(this)

        if (PrefUtil.getString(mContext, AppConstants.PREF_Address_Id, AppConstants.PREF_NAME) != null)
        getData(Event.getAddress)

        return rootView
    }

    override fun onClick(view: View?) {

        when (view!!.getId()) {
            R.id.saveBtn -> {
                if (validateUi()){
                    getData(Event.address)
                }


            }
        }
    }

    /**
     * Validate the fields
     *
     * @return
     */
    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
        mForm.addField(Field.using(addressLine1).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(addressLine2).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(city).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(state).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(country).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(pincode).validate(NotEmpty.build(mContext)))

        return if (mForm.isValid) true else false
        return false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.getAddress -> {
                var addressId = PrefUtil.getString(mContext, AppConstants.PREF_Address_Id, AppConstants.PREF_NAME)
                RequestManager.addRequest(object : GsonObjectRequest<AddressObjectModel>(
                    WebServices.address + CommonUtils.getUserId(mContext) + "/address/" + addressId, reqHeader, null, AddressObjectModel::class.java, VolleyErrorListener(this, Event.getAddress)
                ) {
                    override fun deliverResponse(response: AddressObjectModel) {
                        updateUi(true, Event.getAddress, response)
                    }
                })
            }

            Event.address -> {
                request = addressRequest()
                CommonUtils.logPrint("Address req is: " + request)
                RequestManager.addRequest(object : GsonObjectRequest<AddressObjectModel>(
                    WebServices.address + CommonUtils.getUserId(mContext) + "/address/", reqHeader, request, AddressObjectModel::class.java, VolleyErrorListener(this, Event.getAddress)
                ) {
                    override fun deliverResponse(response: AddressObjectModel) {
                        updateUi(true, Event.address, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.getAddress -> {
                   var mRootModel = serviceResponse as AddressObjectModel
                    addressModel = mRootModel.data
                    addressLine1.setText(addressModel.addressLine1.toString())
                    addressLine2.setText(addressModel.addressLine2.toString())
                    city.setText(addressModel.city.toString())
                    state.setText(addressModel.state.toString())
                    country.setText(addressModel.country.toString())
                    pincode.setText(addressModel.pincode.toString())

                }

                Event.address -> {
//                   var mRootModel = serviceResponse as AddressObjectModel
                    CommonUtils.showToast(mContext, "Profile updated successfully")
                    getActivity()!!.onBackPressed();

                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    /*Request for login*/
    private fun addressRequest(): String {

        val mModel = AddressDTO()
        mModel.id = addressModel.id
        mModel.addressLine1 = addressLine1.text.toString()
        mModel.addressLine2 = addressLine2.text.toString()
        mModel.city = city.text.toString()
        mModel.state = state.text.toString()
        mModel.country = country.text.toString()
        mModel.pincode = pincode.text.toString()
        mModel.latitude = addressModel.latitude
        mModel.longitude = addressModel.longitude
        mModel.patch = "patchMethod"

        mModel.createdBy = CommonUtils.getUserName(mContext)
        mModel.createdOn = CommonUtils.getDateString()
        mModel.modifiedBy = CommonUtils.getUserName(mContext)
        mModel.modifiedOn = CommonUtils.getDateString()
        return Gson().toJson(mModel)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddressFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddressFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
