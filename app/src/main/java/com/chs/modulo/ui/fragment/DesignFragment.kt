package com.chs.modulo.ui.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CustomeSearchView
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.adapter.DesignAdapter
import com.chs.modulo.adapter.SizeAdapter
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.ResponceModel
import com.chs.modulo.model.SizeRootModel
import kotlinx.android.synthetic.main.fragment_design.view.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [DesignFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [DesignFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DesignFragment : ModuloBaseFragment(), CustomeSearchView.OnSearchListener {
    // TODO: Rename and change types of parameters
    private var comingFrom: String? = null
    private var commonApi: String = ""
    private var param2: String? = null
//    private var comingFrom : String? = ""
    private var listener: OnFragmentInteractionListener? = null
    private var rootView: View? = null
    private var mContext: Context? = null
    private var searchString: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            comingFrom = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_design, null, false)
        mContext = activity
        setToolBarHeading(comingFrom)

        if (comingFrom.equals(getString(R.string.design))){
            commonApi = WebServices.design
            getData(Event.design)

        }else if (comingFrom.equals(getString(R.string.shades))){
            commonApi = WebServices.shade
            getData(Event.design)

        }else if (comingFrom.equals(getString(R.string.kinds))){
            commonApi = WebServices.kind
            getData(Event.design)

        }else if (comingFrom.equals(getString(R.string.sizes))){
            getData(Event.size)

        }

        rootView!!.clearBtn.setOnClickListener(View.OnClickListener {
            commonApi = WebServices.design
            getData(Event.design)
            rootView!!.clearBtn.visibility = View.GONE
        })

        rootView!!.fab_add.setOnClickListener(View.OnClickListener {
            if (comingFrom.equals(getString(R.string.design))){
                replaceFragment(AddDesignFragment(), null, true, true, true, null, R.id.container)

            }else if (comingFrom.equals(getString(R.string.shades))){
                replaceFragment(AddShadeFragment(), null, true, true, true, null, R.id.container)

            }else if (comingFrom.equals(getString(R.string.kinds))){
                replaceFragment(AddKindFragment(), null, true, true, true, null, R.id.container)

            }else if (comingFrom.equals(getString(R.string.sizes))){
                replaceFragment(AddSizeFragment(), null, true, true, true, null, R.id.container)
            }
        })

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu);

        if (!comingFrom.equals(getString(R.string.design)))
            menu.findItem(R.id.action_search).isVisible = false

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                val mSearchView = CustomeSearchView()
                mSearchView.loadToolBarSearch(mContext, "Search code")
                mSearchView.setOnSearchListener(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /*Inter face for search Customer */
    override fun setSearchListener(status: String?, mSearchString: String) {
        searchString = mSearchString
        commonApi = WebServices.design + "searchText?keyword=" + searchString
        getData(Event.design)
        rootView!!.clearBtn.visibility = View.VISIBLE
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.design -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    commonApi, reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.design)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.design, response)
                    }
                })
            }
            Event.size -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<SizeRootModel>(
                    WebServices.size, reqHeader, null, SizeRootModel::class.java, VolleyErrorListener(this, Event.size)
                ) {
                    override fun deliverResponse(response: SizeRootModel) {
                        updateUi(true, Event.size, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.design -> {
                    var mdesignModel = serviceResponse as ResponceModel
                    if (mdesignModel != null && mdesignModel.data != null && mdesignModel.data.size > 0){
                        rootView!!.mRecyclerview.visibility = View.VISIBLE
                    val mAdapter = DesignAdapter(mContext, mdesignModel.data, comingFrom)
                    rootView!!.mRecyclerview.adapter = mAdapter

                    }else {
                        rootView!!.mRecyclerview.visibility = View.GONE
                        CommonUtils.showToast(mContext, "NO customers to display")
                    }
                }

                Event.size -> {
                    var mSizeModel = serviceResponse as SizeRootModel

                    if (mSizeModel != null && mSizeModel.data != null && mSizeModel.data.size > 0){
                    val mAdapter = SizeAdapter(mContext, mSizeModel.data)
                    rootView!!.mRecyclerview.adapter = mAdapter
                    }else {
                        rootView!!.mRecyclerview.visibility = View.GONE
                        CommonUtils.showToast(mContext, "NO customers to display")
                    }
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DesignFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DesignFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
