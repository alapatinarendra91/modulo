package com.chs.modulo.ui.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.annotation.Nullable
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CustomeSearchView
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.adapter.CustomersAdapter
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.CustomerRootModel
import kotlinx.android.synthetic.main.fragment_orderslist.view.*
import java.util.*

class CustomersFragment : ModuloBaseFragment(), CustomersAdapter.OnCartChangedListener, CustomeSearchView.OnSearchListener{
    private var rootView: View? = null
    private var mContext: Context? = null
    private var searchString: String = ""
    private var commonApi: String = WebServices.customer
    lateinit var mCustomersModel: CustomerRootModel

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_orderslist, null, false)
        mContext = activity

        setToolBarHeading("Customers")

        getData(Event.customer)

        rootView!!.clearBtn.setOnClickListener(View.OnClickListener {
            commonApi = WebServices.customer
            getData(Event.customer)
            rootView!!.clearBtn.visibility = View.GONE
        })

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu);

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                val mSearchView = CustomeSearchView()
                mSearchView.loadToolBarSearch(mContext, "Search company")
                mSearchView.setOnSearchListener(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    /*Inter face for search Customer */
    override fun setSearchListener(status: String?, mSearchString: String) {
        searchString = mSearchString
        commonApi = WebServices.customer + "searchText?keyword=" + searchString
        getData(Event.customer)
        rootView!!.clearBtn.visibility = View.VISIBLE
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.customer -> {
                RequestManager.addRequest(object : GsonObjectRequest<CustomerRootModel>(
                    commonApi, reqHeader, null, CustomerRootModel::class.java, VolleyErrorListener(this, Event.customer)
                ) {
                    override fun deliverResponse(response: CustomerRootModel) {
                        updateUi(true, Event.customer, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.customer -> {
                    mCustomersModel = serviceResponse as CustomerRootModel
                    if (mCustomersModel != null && mCustomersModel.data != null && mCustomersModel.data.size > 0){
                        rootView!!.mRecyclerview.visibility = View.VISIBLE
                        val mAdapter = CustomersAdapter(mContext, mCustomersModel.data)
                        rootView!!.mRecyclerview.adapter = mAdapter
                        mAdapter.setOnCartChangedListener(this)
                    }else {
                        rootView!!.mRecyclerview.visibility = View.GONE
                        CommonUtils.showToast(mContext, "NO customers to display")
                    }
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    override fun setCartClickListener(status: String, position: Int) {
        if (status.equals("mobile"))
            CommonUtils.makeCall(mContext, mCustomersModel.data[position].mobileNumber)

        if (status.equals("email")){
            val repEmailIntent =
                Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + mCustomersModel.data[position].emailId))
            repEmailIntent.putExtra(Intent.EXTRA_SUBJECT, "Modulo")
            startActivity(Intent.createChooser(repEmailIntent, "Chooser Title"))
        }
    }
}