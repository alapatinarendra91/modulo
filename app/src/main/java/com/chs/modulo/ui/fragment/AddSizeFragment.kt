package com.chs.modulo.ui.fragment

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.NotEmpty
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.SizeMasterDTO
import com.chs.modulo.model.SizeObjectModel
import com.chs.modulo.model.UserDTO
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_add_size.view.*

/**
 * A simple [Fragment] subclass.
 */
class AddSizeFragment: ModuloBaseFragment() {
    private var rootView: View? = null
    private var mContext: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_add_size, container, false)
        mContext = activity

        rootView!!.saveBtn.setOnClickListener(View.OnClickListener {
            if (validateUi())
                getData(Event.size)

        })

        return rootView
    }

    /**
     * Validate the fields
     *
     * @return
     */
    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
//        mForm.addField(Field.using(rootView!!.codeEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(rootView!!.widthInMmEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(rootView!!.heightInMmEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(rootView!!.widthInInEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(rootView!!.heightInInEdt).validate(NotEmpty.build(mContext)))
        return if (mForm.isValid) true else false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.size -> {
                request = addSizeRequest()
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<SizeObjectModel>(
                    WebServices.size,reqHeader, request, SizeObjectModel::class.java, VolleyErrorListener(this, Event.size)
                ) {
                    override fun deliverResponse(response: SizeObjectModel) {
                        updateUi(true, Event.size, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.size -> {
                    var mLoginModel = serviceResponse as SizeObjectModel
                    CommonUtils.showToast(mContext, "Added successfully")

                    rootView!!.codeEdt.setText("")
                    rootView!!.widthInMmEdt.setText("")
                    rootView!!.heightInMmEdt.setText("")
                    rootView!!.widthInInEdt.setText("")
                    rootView!!.heightInInEdt.setText("")

                }
            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            if (errorResponce.message.toLowerCase().contains(getString(R.string.dupilicateCheck))){
                CommonUtils.showToastLong(mContext, "Width In Mm and Height In Mm should be unique in the master info")
                return
            }
            CommonUtils.showToastLong(mContext, "Failed to add")
        }
    }

    /*Request for login*/
    private fun addSizeRequest(): String {

        val mModel = SizeMasterDTO()
//        mModel.code = rootView!!.codeEdt.text.toString()
        mModel.code = rootView!!.widthInMmEdt.text.toString() + " * " + rootView!!.heightInMmEdt.text.toString()
        mModel.widthInMm = rootView!!.widthInMmEdt.text.toString().toInt()
        mModel.heightInMm = rootView!!.heightInMmEdt.text.toString().toInt()
        mModel.widthInIn = rootView!!.widthInInEdt.text.toString().toInt()
        mModel.heightInIn = rootView!!.heightInInEdt.text.toString().toInt()

        mModel.createdBy = CommonUtils.getUserName(mContext)
        mModel.createdOn = CommonUtils.getDateString()
        mModel.modifiedBy = CommonUtils.getUserName(mContext)
        mModel.modifiedOn = CommonUtils.getDateString()
        return Gson().toJson(mModel)
    }

}
