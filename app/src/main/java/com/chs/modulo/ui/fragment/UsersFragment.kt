package com.chs.modulo.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.chs.modulo.R
import com.chs.modulo.model.ResponceModel
import kotlinx.android.synthetic.main.fragment_items.view.*
import java.util.ArrayList

class UsersFragment : ModuloBaseFragment(){
    private var rootView: View? = null
    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_items, null, false)
        mContext = activity

        setToolBarHeading("Users")

        val mData: List<ResponceModel.Data> = ArrayList()

//        val mAdapter = DesignAdapter(mContext, "users")
//        rootView!!.mRecyclerview.adapter = mAdapter

//        rootView!!.fab_add.setOnClickListener(View.OnClickListener {
//            replaceFragment(AddUserFragment(), null, true, true, true, null, R.id.container)
//        })

        return rootView
    }
}