package com.warehouse.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Html
import android.view.View
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.IsEmail
import chs.validateui.NotEmpty
import com.chs.modulo.R
import com.chs.modulo.Utils.AlertDialogHelper
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CommonUtils.logPrint
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.*
import com.chs.modulo.ui.activity.ForgotPasswordActivity
import com.chs.modulo.ui.activity.MainActivity
import com.chs.modulo.ui.activity.ModuloBaseActivity
import com.chs.modulo.ui.activity.RegisterActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import java.util.ArrayList

class LoginActivity : ModuloBaseActivity(), View.OnClickListener, AlertDialogHelper.AlertDialogListener {
    private var mContext: Context? = null
    private var alertDialog: AlertDialogHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mContext = this
        initViews()
//        userNameEdit.setText("narendra")
//        passwordEdt.setText("agent123")

        forgotPasswordText.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

    }

    private fun initViews() {
        setToolBarHeading("Login")

        alertDialog = AlertDialogHelper(mContext)
        alertDialog!!.setOnAlertListener(this)

        loginBtn.setOnClickListener(this)
        forgotPasswordText.setOnClickListener(this)
        newCustomerSignUpBtn.setOnClickListener(this)

        if (PrefUtil.getBool(
                this@LoginActivity,AppConstants.PREF_RememberMe,AppConstants.PREF_NAME)) {
            userNameEdit.setText(PrefUtil.getString(this@LoginActivity,AppConstants.PREF_UserName,AppConstants.PREF_NAME))
            passwordEdt.setText(PrefUtil.getString(this@LoginActivity, AppConstants.PREF_Password,AppConstants.PREF_NAME))
            rememberCheck.isChecked = true
            userNameEdit.setSelection(userNameEdit.getText().toString().length)
            passwordEdt.setSelection(passwordEdt.text.toString().length)
        }
    }



    override fun onClick(view: View?) {

        when (view!!.getId()) {
            R.id.loginBtn -> {
                if (validateUi())
                    getData(Event.login)

            }

            R.id.forgotPasswordText -> {
                startActivity(Intent(mContext, ForgotPasswordActivity::class.java))

//                val mForm = Form(mContext as Activity?)
//                mForm.addField(Field.using(userNameEdit).validate(NotEmpty.build(mContext)).validate(IsEmail.build(mContext)))
//
//                if (!mForm.isValid)
//                    return
//
//                alertDialog?.showAlertDialog("Forgot Password","Are you sure you want to get your password to registered email?",
//                    "Yes","No",Event.forgot,true)
            }

            R.id.newCustomerSignUpBtn -> {
                startActivity(Intent(mContext, RegisterActivity::class.java))
            }
        }
    }

    /**
     * Validate the fields
     *
     * @return
     */
    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
        mForm.addField(Field.using(userNameEdit).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(passwordEdt).validate(NotEmpty.build(mContext)))
        return if (mForm.isValid) true else false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        var request = ""

        when (actionID) {
            Event.login -> {
                request = loginRequest()
                logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<ItemObjectRootModel>(
                    WebServices.login,
                    reqHeader, request, ItemObjectRootModel::class.java, VolleyErrorListener(this, Event.login)
                ) {
                    override fun deliverResponse(response: ItemObjectRootModel) {
                        updateUi(true, Event.login, response)
                    }
                })
            }

            Event.design -> {
                logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<UserDTO>(
                    WebServices.design,reqHeader, null, UserDTO::class.java, VolleyErrorListener(this, Event.design)) {
                    override fun deliverResponse(response: UserDTO) {
                        updateUi(true, Event.design, response)
                    }
                })
            }

            Event.forgot -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    WebServices.forgot + userNameEdit.text,
                    reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.forgot)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.forgot, response)
                    }
                })
            }
        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.login -> {
                    var mLoginModel = serviceResponse as ItemObjectRootModel

                    PrefUtil.clearAllSharedPrefarences(mContext, AppConstants.PREF_NAME)

                    if (mLoginModel.userDTO.role == null || !mLoginModel.userDTO.role.code.toUpperCase().equals("ADMIN")){
                        CommonUtils.showToast(mContext, "Please login as admin")
                        return
                    }

                    if (mLoginModel.userDTO.address != null)
                        PrefUtil.putString(mContext, AppConstants.PREF_Address_Id, "" + mLoginModel.userDTO.address.id ,AppConstants.PREF_NAME)

                    PrefUtil.putString(mContext, AppConstants.PREF_UserID, "" + mLoginModel.userDTO.id ,AppConstants.PREF_NAME)
                    PrefUtil.putString(mContext, AppConstants.PREF_UserName, "" + mLoginModel.userDTO.userName ,AppConstants.PREF_NAME)
                    PrefUtil.putString(mContext, AppConstants.PREF_MobileNo, "" + mLoginModel.userDTO.mobileNumber ,AppConstants.PREF_NAME)
                    PrefUtil.putString(mContext, AppConstants.PREF_DisplayName, "" + mLoginModel.userDTO.firstName + " " + mLoginModel.userDTO.lastName ,AppConstants.PREF_NAME)
                    PrefUtil.putString(mContext, AppConstants.PREF_Password, "" + passwordEdt.text.toString() ,AppConstants.PREF_NAME)
                    PrefUtil.putString(mContext, AppConstants.PREF_Email, "" + mLoginModel.userDTO.emailId ,AppConstants.PREF_NAME)
                    PrefUtil.putString(mContext, AppConstants.PREF_Auth_token, "Bearer " + mLoginModel.token ,AppConstants.PREF_NAME)
                    PrefUtil.putBool(mContext, AppConstants.PREF_RememberMe, rememberCheck.isChecked ,AppConstants.PREF_NAME)
                    startActivity(Intent(mContext, MainActivity::class.java).putExtra(AppConstants.Intent_resetPasswordReqd, mLoginModel.userDTO.resetPasswordReqd))
                    finish()

                }
                Event.design -> {
                    var mdesignModel = serviceResponse as UserDTO
                    startActivity(Intent(mContext, MainActivity::class.java))

                }
                Event.forgot -> {
                    var mForgotModel = serviceResponse as ResponceModel
                    alertDialog?.showAlertDialog("Forgot Password",
                        mForgotModel.statusMessage,
                        "Ok",
                        Event.ExtraDialodParam,
                        true
                    )
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, "Please check your user name / Password")
        }
    }

    /*Request for login*/
    private fun loginRequest(): String {

            val mModel = RequestModel()
            mModel.userName = userNameEdit.text.toString()
            mModel.password = passwordEdt.text.toString()
//            mModel.patch = "patchMethod"

//        mModel.createdBy = CommonUtils.getUserName(mContext)
//        mModel.createdOn = CommonUtils.getDateString()
//        mModel.modifiedBy = CommonUtils.getUserName(mContext)
//        mModel.modifiedOn = CommonUtils.getDateString()

        return Gson().toJson(mModel)
    }


    /**
     * This method is invoked when the positive button is clicked
     * @param from
     */
    override fun onPositiveClick(from: Int) {
        if (from == Event.forgot)
            getData(Event.forgot)

    }

    /**
     * This method is invoked when the negative button is clicked
     * @param from
     */
    override fun onNegativeClick(from: Int) {

    }

    /**
     * This method is invoked when the neutral button is clicked
     * @param from
     */
    override fun onNeutralClick(from: Int) {

    }

    override fun onBackPressed() {
    }

}