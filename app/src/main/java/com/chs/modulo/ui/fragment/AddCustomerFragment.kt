package com.chs.modulo.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.chs.modulo.R

class AddCustomerFragment : ModuloBaseFragment(){
    private var rootView: View? = null
    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.activity_add_customer, null, false)
        mContext = activity

        setToolBarHeading("Add Customer")

        return rootView
    }
}