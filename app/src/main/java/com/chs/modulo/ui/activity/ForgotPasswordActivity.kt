package com.chs.modulo.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.IsEmail
import chs.validateui.NotEmpty
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.ResponceModel
import com.chs.modulo.model.UserDTO
import kotlinx.android.synthetic.main.activity_forgotpassword.*
import kotlinx.android.synthetic.main.activity_login.*

class ForgotPasswordActivity : ModuloBaseActivity() {
    private var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgotpassword)
        mContext = this

        setToolBarHeading("Forgot Password", true)

        submitBtn.setOnClickListener(View.OnClickListener {
            val mForm = Form(mContext as Activity?)
            mForm.addField( Field.using(emailEdt).validate(NotEmpty.build(mContext)).validate(IsEmail.build(mContext)))

            if (!mForm.isValid)
                return@OnClickListener

            getData(Event.forgot)
        })
    }

    /**
     * actions for the menu item selection
     *
     * @param item
     * @return
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        var request = ""

        when (actionID) {
            Event.forgot -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    WebServices.forgot + emailEdt.text,
                    reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.forgot)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.forgot, response)
                    }
                })
            }
        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.forgot -> {
                    var mForgotModel = serviceResponse as ResponceModel
                    CommonUtils.showToast(mContext, "Your password will be send to your mail id")
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, "Please check your email id")
        }
    }
}