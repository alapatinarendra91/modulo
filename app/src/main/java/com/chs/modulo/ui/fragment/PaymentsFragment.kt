package com.chs.modulo.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.adapter.PaymentsAdapter
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.PaymentReceiptRootModel
import kotlinx.android.synthetic.main.fragment_orderslist.view.*
import java.util.HashMap

class PaymentsFragment : ModuloBaseFragment(){
    private var rootView: View? = null
    private var mContext: Context? = null
    lateinit var mPaymentModel: PaymentReceiptRootModel

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_orderslist, null, false)
        mContext = activity

        setToolBarHeading("Payments")

        getData(Event.payment)

        return rootView
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.payment -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<PaymentReceiptRootModel>(
                    WebServices.payment, reqHeader, null, PaymentReceiptRootModel::class.java, VolleyErrorListener(this, Event.payment)
                ) {
                    override fun deliverResponse(response: PaymentReceiptRootModel) {
                        updateUi(true, Event.payment, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.payment -> {
                    mPaymentModel = serviceResponse as PaymentReceiptRootModel
                    val mAdapter = PaymentsAdapter(mContext, mPaymentModel.data)
                    rootView!!.mRecyclerview.adapter = mAdapter
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

}