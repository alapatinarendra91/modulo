package com.chs.modulo.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.annotation.Nullable
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CustomeSearchView
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.adapter.OrdersAdapter
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.OrdersRootModel
import kotlinx.android.synthetic.main.fragment_orderslist.view.*
import java.util.HashMap

class OrdersListFragment : ModuloBaseFragment(), OrdersAdapter.OnCartChangedListener, CustomeSearchView.OnSearchListener{
    private var rootView: View? = null
    private var mContext: Context? = null
    private var searchString: String = ""
    private var commonApi: String = WebServices.order
    lateinit var mOrdersModel: OrdersRootModel

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_orderslist, null, false)
        mContext = activity

        setToolBarHeading("Orders")
        getData(Event.order)

        rootView!!.clearBtn.setOnClickListener(View.OnClickListener {
            commonApi = WebServices.order
            getData(Event.order)
            rootView!!.clearBtn.visibility = View.GONE
        })

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu);

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                val mSearchView = CustomeSearchView()
                mSearchView.loadToolBarSearch(mContext, "Search order id")
                mSearchView.setOnSearchListener(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /*Inter face for search Order */
    override fun setSearchListener(status: String?, mSearchString: String) {
        searchString = mSearchString
        commonApi = WebServices.order + "searchText?keyword=" + searchString
        getData(Event.order)
        rootView!!.clearBtn.visibility = View.VISIBLE
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.order -> {
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<OrdersRootModel>(
                    commonApi, reqHeader, null, OrdersRootModel::class.java, VolleyErrorListener(this, Event.order)
                ) {
                    override fun deliverResponse(response: OrdersRootModel) {
                        updateUi(true, Event.order, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.order -> {
                    mOrdersModel = serviceResponse as OrdersRootModel
                    if (mOrdersModel != null && mOrdersModel.data != null && mOrdersModel.data.size > 0){
                        rootView!!.mRecyclerview.visibility = View.VISIBLE
                    val mAdapter = OrdersAdapter(mContext, mOrdersModel.data)
                    rootView!!.mRecyclerview.adapter = mAdapter
                    mAdapter.setOnCartChangedListener(this)

                    }else {
                        rootView!!.mRecyclerview.visibility = View.GONE
                        CommonUtils.showToast(mContext, "NO customers to display")
                    }
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    override fun setCartClickListener(status: String, position: Int) {
//        if (status.equals("itemView")){
//            var  mBundle =  Bundle()
//            mBundle.putString("Id", mOrdersModel.data[position].id)
//            replaceFragment(ProductDescriptionFragment(), mBundle, true, true, true, null, R.id.container)
//        }
    }
}