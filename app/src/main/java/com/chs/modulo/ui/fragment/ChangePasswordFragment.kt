package com.chs.modulo.ui.fragment

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.IsEmail
import chs.validateui.NotEmpty

import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.RequestModel
import com.chs.modulo.model.SizeRootModel
import com.chs.modulo.model.SuccessModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_change_password.*
import kotlinx.android.synthetic.main.fragment_change_password.view.*
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ChangePasswordFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ChangePasswordFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChangePasswordFragment : ModuloBaseFragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var rootView: View? = null
    private var mContext: Context? = null
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_change_password, container, false)
        mContext = activity
        setToolBarHeading("Change Password")
        rootView!!.saveBtn.setOnClickListener(this)

        return rootView
    }

    override fun onClick(view: View?) {

        when (view!!.getId()) {
            R.id.saveBtn -> {
                if (validateUi()){
                    getData(Event.changePassword)
                }


            }
        }
    }

    /**
     * Validate the fields
     *
     * @return
     */
    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
        mForm.addField(Field.using(oldPasswordEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(newPasswordEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(confirmPasswordEdt).validate(NotEmpty.build(mContext)))

        if (mForm .isValid && !newPasswordEdt.text.toString().equals(confirmPasswordEdt.text.toString())){
            CommonUtils.showToastLong(mContext, "Please check password and Confirm password")
            return false
        }

        return if (mForm.isValid) true else false
        return false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.changePassword -> {
                request = changePasswordRequest()
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<SuccessModel>(
                    WebServices.changePassword, reqHeader, request, SuccessModel::class.java, VolleyErrorListener(this, Event.changePassword)
                ) {
                    override fun deliverResponse(response: SuccessModel) {
                        updateUi(true, Event.changePassword, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.changePassword -> {
                    var mSizeModel = serviceResponse as SuccessModel
                    CommonUtils.showToast(mContext, "Password changed successfully")
                    getActivity()!!.onBackPressed();

                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    /*Request for login*/
    private fun changePasswordRequest(): String {

        val mModel = RequestModel()
        mModel.userId = CommonUtils.getUserId(mContext)
        mModel.oldPassword = oldPasswordEdt.text.toString()
        mModel.newPassword = newPasswordEdt.text.toString()
        return Gson().toJson(mModel)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChangePasswordFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ChangePasswordFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
