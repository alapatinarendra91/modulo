package com.chs.modulo.ui.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.NotEmpty
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CommonUtils.showToast
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.*
import com.chs.modulo.ui.activity.MainActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.add_desin.*
import kotlinx.android.synthetic.main.add_desin.view.*
import kotlinx.android.synthetic.main.add_desin.view.codeEdt
import kotlinx.android.synthetic.main.add_desin.view.descEdt
import kotlinx.android.synthetic.main.add_desin.view.saveBtn
import kotlinx.android.synthetic.main.fragment_add_shade.view.*

class AddDesignFragment : ModuloBaseFragment(){
    private var rootView: View? = null
    private var mContext: Context? = null

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.add_desin, null, false)

        mContext = activity

        rootView!!.saveBtn.setOnClickListener(View.OnClickListener {
            if (validateUi())
                getData(Event.design)

        })

        return rootView
    }

    /**
     * Validate the fields
     *
     * @return
     */
    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
        mForm.addField(Field.using(rootView!!.codeEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(rootView!!.descEdt).validate(NotEmpty.build(mContext)))
        return if (mForm.isValid) true else false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.design -> {
                request = addDesignRequest()
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<DesignObjectModel>(
                    WebServices.design,reqHeader, request, DesignObjectModel::class.java, VolleyErrorListener(this, Event.design)
                ) {
                    override fun deliverResponse(response: DesignObjectModel) {
                        updateUi(true, Event.design, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.design -> {
                    var mLoginModel = serviceResponse as DesignObjectModel
                    showToast(mContext, "Added successfully")

                    rootView!!.codeEdt.setText("")
                    rootView!!.descEdt.setText("")

                }
            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            if (errorResponce.message.toLowerCase().contains(getString(R.string.dupilicateCheck))){
                CommonUtils.showToastLong(mContext, "Width In Mm and Height In Mm should be unique in the master info")
                return
            }
            CommonUtils.showToastLong(mContext, "Failed to add")
        }
    }

    /*Request for login*/
    private fun addDesignRequest(): String {

        val mModel = DesignMasterDTO()
        mModel.id = 0
        mModel.code = rootView!!.codeEdt.text.toString()
        mModel.description = rootView!!.descEdt.text.toString()
        mModel.status = getString(R.string.active)

        mModel.createdBy = CommonUtils.getUserName(mContext)
        mModel.createdOn = CommonUtils.getDateString()
        mModel.modifiedBy = CommonUtils.getUserName(mContext)
        mModel.modifiedOn = CommonUtils.getDateString()
        return Gson().toJson(mModel)
    }


}