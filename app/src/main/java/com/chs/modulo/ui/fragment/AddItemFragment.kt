package com.chs.modulo.ui.fragment

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MultiAutoCompleteTextView.CommaTokenizer
import androidx.annotation.Nullable
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.NotEmpty
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.Utils.SearchableAdapter
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_add_item.*
import kotlinx.android.synthetic.main.fragment_add_item.view.*
import kotlinx.android.synthetic.main.fragment_product_details.*
import java.util.*
import kotlin.collections.ArrayList

class AddItemFragment : ModuloBaseFragment(){
    private var rootView: View? = null
    private var mContext: Context? = null
    lateinit var mItemsModel: ItemObjectRootModel
    lateinit var mDesignModel: DesignRootModel
    lateinit var mShadeModel: ShadeRootModel
    lateinit var mKindModel: KindRootModel
    lateinit var mSizeModel: SizeRootModel
    private var add_editType: String? = ""
    private var productId: String? = ""
    private var designPos: Int = -1
    private var shadePos: Int = -1
    private var kindPos: Int = -1
    private var sizePos: Int = -1

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_add_item, null, false)

        mContext = activity
        add_editType = arguments!!.getString("Type_Add_Edit")

        if (add_editType.equals("EDIT")){
            productId = arguments!!.getString("Id")
            rootView!!.addItemBtn.setText("Update")
            getData(Event.item)
        }

        setToolBarHeading(if (add_editType.equals("ADD")) "Add Item" else "Edit item")

//        rootView!!.designCodeEdt.threshold = 1
//        rootView!!.designCodeEdt.setTokenizer(CommaTokenizer())
//        rootView!!.shadeCodeEdt.setTokenizer(CommaTokenizer())
//        rootView!!.kindCodeEdt.setTokenizer(CommaTokenizer())
//        rootView!!.sizeCodeEdt.setTokenizer(CommaTokenizer())

        var qualityTypeList = ArrayList<String>()
        qualityTypeList.add("PREMIUM")
        qualityTypeList.add("STANDARD")
        qualityTypeList.add("ECONOMY")
        qualityTypeList.add("COMMERCIAL")

        rootView!!.orderQualityEdt.setTokenizer(CommaTokenizer())
        val mAdapter = SearchableAdapter(mContext, qualityTypeList)
        rootView!!.orderQualityEdt.setAdapter(mAdapter)

        getData(Event.design)
        getData(Event.shade)
        getData(Event.kind)
        getData(Event.size)

        rootView!!.addItemBtn.setOnClickListener(View.OnClickListener {

             designPos = -1
             shadePos = -1
             kindPos = -1
             sizePos = -1

            if (validateUi()){

                /*Getting design pos*/
                for (i in 0 until mDesignModel.data.size){
                    if (designCodeEdt.text.toString().trim().toLowerCase().equals(mDesignModel.data.get(i).code.toLowerCase())){
                        designPos = i
                        break
                    }
                }

                /*Getting size pos*/
                for (i in 0 until mSizeModel.data.size){
                    if (sizeCodeEdt.text.toString().trim().toLowerCase().equals(mSizeModel.data.get(i).code.toLowerCase())){
                        sizePos = i
                        break
                    }
                }

//                /*Getting shade pos*/
//                for (i in 0 until mShadeModel.data.size){
//                    if (shadeCodeEdt.text.toString().toLowerCase().equals(mShadeModel.data.get(i).code.toLowerCase())){
//                        shadePos = i
//                        break
//                    }
//                }
//
//                /*Getting kind pos*/
//                for (i in 0 until mKindModel.data.size){
//                    if (kindCodeEdt.text.toString().toLowerCase().equals(mKindModel.data.get(i).code.toLowerCase())){
//                        kindPos = i
//                        break
//                    }
//                }


                if (designPos == -1){
                    CommonUtils.showToast(mContext, "Please Check Design Code")
                    return@OnClickListener
                }
                if (sizePos == -1){
                    CommonUtils.showToast(mContext, "Please Check Size Code")
                    return@OnClickListener
                }

                getData(Event.updateItem)

            }
        })


        return rootView
    }

    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
        mForm.addField(Field.using(designCodeEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(shadeCodeEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(kindCodeEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(sizeCodeEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(orderQualityEdt).validate(NotEmpty.build(mContext)))

        if (mForm.isValid && rootView!!.brandTypeSpin.selectedItemPosition == 0){
            CommonUtils.showToast(mContext, "Please select brand type")
            return false
        }
        return if (mForm.isValid) true else false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.item -> {
                RequestManager.addRequest(object : GsonObjectRequest<ItemObjectRootModel>(
                    WebServices.item + productId, reqHeader, null, ItemObjectRootModel::class.java, VolleyErrorListener(this, Event.item)
                ) {
                    override fun deliverResponse(response: ItemObjectRootModel) {
                        updateUi(true, Event.item, response)
                    }
                })
            }

            Event.updateItem -> {
                request = updateItemRequest()
                CommonUtils.logPrint("updateItem Req is: " + request)

                RequestManager.addRequest(object : GsonObjectRequest<ItemObjectRootModel>(
                    WebServices.item,
                    reqHeader,
                    request,
                    ItemObjectRootModel::class.java,
                    VolleyErrorListener(this, Event.updateItem)
                ) {
                    override fun deliverResponse(response: ItemObjectRootModel) {
                        updateUi(true, Event.updateItem, response)
                    }
                })
            }

            Event.design -> {
                RequestManager.addRequest(object : GsonObjectRequest<DesignRootModel>(
                    WebServices.design, reqHeader, null, DesignRootModel::class.java, VolleyErrorListener(this, Event.design)
                ) {
                    override fun deliverResponse(response: DesignRootModel) {
                        updateUi(true, Event.design, response)
                    }
                })
            }

            Event.shade -> {
                RequestManager.addRequest(object : GsonObjectRequest<ShadeRootModel>(
                    WebServices.shade, reqHeader, null, ShadeRootModel::class.java, VolleyErrorListener(this, Event.shade)
                ) {
                    override fun deliverResponse(response: ShadeRootModel) {
                        updateUi(true, Event.shade, response)
                    }
                })
            }

            Event.kind -> {
                RequestManager.addRequest(object : GsonObjectRequest<KindRootModel>(
                    WebServices.kind, reqHeader, null, KindRootModel::class.java, VolleyErrorListener(this, Event.kind)
                ) {
                    override fun deliverResponse(response: KindRootModel) {
                        updateUi(true, Event.kind, response)
                    }
                })
            }

            Event.size -> {
                RequestManager.addRequest(object : GsonObjectRequest<SizeRootModel>(
                    WebServices.size, reqHeader, null, SizeRootModel::class.java, VolleyErrorListener(this, Event.size)
                ) {
                    override fun deliverResponse(response: SizeRootModel) {
                        updateUi(true, Event.size, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.item -> {
                    mItemsModel = serviceResponse as ItemObjectRootModel
                    setToolBarHeading(mItemsModel.data.label)

                    designCodeEdt.setText("" + mItemsModel.data.design.code)
                    shadeCodeEdt.setText("" + mItemsModel.data.shade)
                    kindCodeEdt.setText("" + mItemsModel.data.kind)
                    sizeCodeEdt.setText("" + mItemsModel.data.size.code)
                    orderQualityEdt.setText("" + mItemsModel.data.qualities)
                    descriptionEdt.setText("" + if (mItemsModel.data.description == null) "" else mItemsModel.data.description)

                   val brandType = if (mItemsModel.data.brand == null) "" else mItemsModel.data.brand
                    if (brandType.toUpperCase().equals("MODULO")){
                        rootView!!.brandTypeSpin.setSelection(1)
                    }else if (brandType.toUpperCase().equals("QUADRO")){
                        rootView!!.brandTypeSpin.setSelection(2)
                    }else if (brandType.toUpperCase().equals("GRANDIOSE")){
                        rootView!!.brandTypeSpin.setSelection(3)
                    }


                    val type = if (mItemsModel.data.type == null) "" else mItemsModel.data.type
                    if (type.toUpperCase().equals("GLOSSY"))
                        rootView!!.glossyRadio.isChecked = true
                    else
                        rootView!!.mattRadio.isChecked = true

                }

                Event.updateItem ->{
                    mItemsModel = serviceResponse as ItemObjectRootModel
                    val message = if (add_editType.equals("ADD")) "Item Added Successfully" else "Item Updated Successfully"
                    CommonUtils.showToast(mContext, message)
                    getActivity()!!.onBackPressed();
                }

                Event.design -> {
                    mDesignModel = serviceResponse as DesignRootModel
                    val mDesignList = ArrayList<String>()

                    for (i in 0 until mDesignModel.data.size)
                        mDesignList.add(mDesignModel.data[i].code)

                    val mAdapter = SearchableAdapter(mContext, mDesignList)
                    rootView!!.designCodeEdt.setAdapter(mAdapter)

                }

                Event.shade -> {
                    mShadeModel = serviceResponse as ShadeRootModel
                    val mShadeList = ArrayList<String>()

                    for (i in 0 until mShadeModel.data.size)
                        mShadeList.add(mShadeModel.data[i].code)

                    val mAdapter = SearchableAdapter(mContext, mShadeList)
                    rootView!!.shadeCodeEdt.setAdapter(mAdapter)

                }

                Event.kind -> {
                    mKindModel = serviceResponse as KindRootModel
                    val mKindList = ArrayList<String>()

                    for (i in 0 until mKindModel.data.size)
                        mKindList.add(mKindModel.data[i].code)

                    val mAdapter = SearchableAdapter(mContext, mKindList)
                    rootView!!.kindCodeEdt.setAdapter(mAdapter)

                }
                Event.size -> {
                    mSizeModel = serviceResponse as SizeRootModel
                    val mSizeList = ArrayList<String>()

                    for (i in 0 until mSizeModel.data.size)
                        mSizeList.add(mSizeModel.data[i].code)

                    val mAdapter = SearchableAdapter(mContext, mSizeList)
                    rootView!!.sizeCodeEdt.setAdapter(mAdapter)

                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            if (errorResponce.message.toLowerCase().contains(getString(R.string.dupilicateCheck))){
                CommonUtils.showToastLong(mContext, "Width In Mm and Height In Mm should be unique in the master info")
                return
            }
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    /*Request for login*/
    private fun updateItemRequest(): String {
        if (add_editType.equals("ADD")){
            mItemsModel = ItemObjectRootModel()
            mItemsModel.data = ItemDTO()
        }
//        mItemsModel.data.design.id = mDesignModel.data[designPos].id
//        mItemsModel.data.design.code = designCodeEdt.text.toString()
//        mItemsModel.data.design.description = mDesignModel.data[designPos].description
//        mItemsModel.data.design.createdBy = mDesignModel.data[designPos].createdBy
//        mItemsModel.data.design.createdOn = mDesignModel.data[designPos].createdOn
//        mItemsModel.data.design.modifiedBy = mDesignModel.data[designPos].modifiedBy
//        mItemsModel.data.design.modifiedOn = mDesignModel.data[designPos].modifiedOn
//        mItemsModel.data.design.designedBy = mDesignModel.data[designPos].designedBy
//        mItemsModel.data.design.status = mDesignModel.data[designPos].status
//
//
//        mItemsModel.data.size.id = mSizeModel.data[sizePos].id
//        mItemsModel.data.size.code = sizeCodeEdt.text.toString()
//        mItemsModel.data.size.widthInMm = mSizeModel.data[sizePos].widthInMm
//        mItemsModel.data.size.heightInMm = mSizeModel.data[sizePos].heightInMm
//        mItemsModel.data.size.widthInIn = mSizeModel.data[sizePos].widthInIn
//        mItemsModel.data.size.heightInIn = mSizeModel.data[sizePos].heightInIn
//        mItemsModel.data.size.createdBy = mSizeModel.data[sizePos].createdBy
//        mItemsModel.data.size.createdOn = mSizeModel.data[sizePos].createdOn
//        mItemsModel.data.size.modifiedBy = mSizeModel.data[sizePos].modifiedBy
//        mItemsModel.data.size.modifiedOn = mSizeModel.data[sizePos].modifiedOn
//        mItemsModel.data.size.status = mSizeModel.data[sizePos].status

        var qualityStr = orderQualityEdt.text.toString().trim()
        var lastChar = qualityStr.substring(qualityStr.length -1)

        mItemsModel.data.design = mDesignModel.data[designPos]
        mItemsModel.data.size = mSizeModel.data[sizePos]

        mItemsModel.data.shade = shadeCodeEdt.text.toString()
        mItemsModel.data.kind = kindCodeEdt.text.toString()
        mItemsModel.data.description = descriptionEdt.text.toString()
        mItemsModel.data.brand = brandTypeSpin.selectedItem.toString()
        mItemsModel.data.type = if (rootView!!.glossyRadio.isChecked) "GLOSSY" else "MATT"
        mItemsModel.data.status = "ACTIVE"
        mItemsModel.data.qualities = if (lastChar.equals(",")) qualityStr.substring(0, qualityStr.length-1) else qualityStr
        mItemsModel.data.createdBy = CommonUtils.getUserName(mContext)
        mItemsModel.data.createdOn = CommonUtils.getDateString()
        mItemsModel.data.modifiedBy = CommonUtils.getUserName(mContext)
        mItemsModel.data.modifiedOn = CommonUtils.getDateString()

        if (add_editType.equals("EDIT")){
            mItemsModel.data.patch = "patchMethod"
        }

        return Gson().toJson(mItemsModel.data)
//        return Gson().toJson(mItemsModel.data).replace("\"status\":", "\"STATUS\":").replace("\"type\":", "\"TYPE\":").replace("\"brand\":", "\"BRAND\":")
    }

}