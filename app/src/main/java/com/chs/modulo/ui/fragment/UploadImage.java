package com.chs.modulo.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.chs.modulo.R;
import com.chs.modulo.Utils.CommonUtils;
import com.chs.modulo.Utils.PrefUtil;
import com.chs.modulo.constants.AppConstants;
import com.chs.modulo.constants.ErrorModel;
import com.chs.modulo.constants.Event;
import com.chs.modulo.constants.VolleyErrorListener;
import com.chs.modulo.interfaces.WebServices;
import com.chs.modulo.model.FileCategoryDTO;
import com.chs.modulo.model.FileDTO;
import com.chs.modulo.model.ResponceModel;
import com.google.gson.Gson;
import com.warehouse.ui.activity.LoginActivity;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import chs.ext.GsonObjectRequest;
import chs.ext.RequestManager;

public class UploadImage extends ModuloBaseFragment{
    private View rootView;
    private Context mContext;
    private ImageView profileImage;
    private Button saveBtn;
    private int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private byte[] byteArray;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_change_profile_image, null, false);
        mContext = getActivity();
        setToolBarHeading("Profile image");

        /* Initializing Views*/
        initViews();

        return rootView;
    }

    private void initViews() {
        profileImage = rootView.findViewById(R.id.profileImage);
        saveBtn = rootView.findViewById(R.id.saveBtn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData(Event.file);
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,
                        CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byteArray = stream.toByteArray();

                // convert byte array to Bitmap

                Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                        byteArray.length);

                profileImage.setImageBitmap(bitmap);

            }
        }
    }

    // gallery

    /**
     * Requesting api calls
     * */
    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<String, String>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        reqHeader.put("Authorization", (PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)));
        String request = "";

        switch (actionID) {
            case Event.file:
                request = uploadImageRequest();
                CommonUtils.logPrint("UploadImage request is:"  + request);
                RequestManager.addRequest(new GsonObjectRequest<FileDTO>
                        (WebServices.file, reqHeader, request,
                                FileDTO.class, new VolleyErrorListener(this, Event.file)) {
                    @Override
                    protected void deliverResponse(FileDTO response) {
                        updateUi(true, Event.file, response);
                    }
                });
                break;

        }
    }

    /*Getting responce from Api's and bind to model class*/
    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.file:
                    FileDTO mFileModel = (FileDTO) serviceResponse;

                    break;
            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "Upload image failed");
        }

    }

    private String uploadImageRequest(){
        FileDTO mRequest = new FileDTO();
        FileCategoryDTO mCategoryModel = new FileCategoryDTO();

        mCategoryModel.setType("IMAGE");
        mCategoryModel.setRelatedCodes("PNG");
        mCategoryModel.setCreatedBy(CommonUtils.getUserName(mContext));
        mCategoryModel.setCreatedOn(CommonUtils.getDateString());
        mCategoryModel.setModifiedBy(CommonUtils.getUserName(mContext));
        mCategoryModel.setModifiedOn(CommonUtils.getDateString());

        mRequest.setCategory(mCategoryModel);

        mRequest.setFileName("fileName");
        mRequest.setSizeInBytes((long) byteArray.length);
        mRequest.setContent("" + Base64.encodeToString(byteArray, Base64.NO_WRAP));
        mRequest.setContentType("IMG");
        mRequest.setCreatedBy(CommonUtils.getUserName(mContext));
        mRequest.setCreatedOn(CommonUtils.getDateString());

        return new Gson().toJson(mRequest);
    }

}
