//package com.warehouse.ui.activity
//
//import android.content.Context
//import android.os.Bundle
//import android.view.MenuItem
//import com.chs.modulo.R
//import com.chs.modulo.constants.AppConstants
//import com.chs.modulo.interfaces.WebServices
//import com.chs.modulo.ui.activity.ModuloBaseActivity
//import com.squareup.picasso.Picasso
//import kotlinx.android.synthetic.main.activity_zoomimage.*
//
//class ZoomImageActivity : ModuloBaseActivity(){
//    private var mContext: Context? = null
//    private var itemNO: String? = null
//    var photoViewAttacher: PhotoViewAttacher? = null
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_zoomimage)
//
//        mContext = this
//        initViews()
//    }
//
//    private fun initViews() {
//        /**
//         *   get required values from the previous activity .
//         * @param itemNo
//         */
//        /**
//         * get required values from the previous activity .
//         * @param itemNo
//         */
//        if (intent != null) {
//            itemNO = intent.extras!!.getString(AppConstants.Intent_itemsNo)
//            setToolBarHeading("Item#: " + itemNO, true)
//            Picasso.with(this).load(WebServices.imageBaseUrl + itemNO + ".jpg")
//                .placeholder(R.mipmap.ic_launcher)
//                .into(image)
//            photoViewAttacher = PhotoViewAttacher(image)
//            photoViewAttacher!!.update()
//        }
//    }
//
//    /**
//     * actions for the menu item selection
//     *
//     * @param item
//     * @return
//     */
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        val id = item.itemId
//        if (id == android.R.id.home) {
//            onBackPressed()
//        }
//        return super.onOptionsItemSelected(item)
//    }
//}