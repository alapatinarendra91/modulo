package com.chs.modulo.ui.fragment

import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CommonUtils.logPrint
import com.chs.modulo.Utils.CustomeSearchView
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.adapter.FilterParentAdapter
import com.chs.modulo.adapter.ItemsAdapter
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.FilterRootModel
import com.chs.modulo.model.ItemsRootModel
import com.chs.modulo.model.RequestModel
import com.chs.modulo.model.ResponceModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.dialog_item_filter.*
import kotlinx.android.synthetic.main.fragment_design.view.mRecyclerview
import kotlinx.android.synthetic.main.fragment_items.view.*
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ItemsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ItemsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ItemsFragment : ModuloBaseFragment(), ItemsAdapter.OnCartChangedListener, CustomeSearchView.OnSearchListener, FilterParentAdapter.OnCartChangedListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var searchString: String = ""
    private var listener: OnFragmentInteractionListener? = null
    private var rootView: View? = null
    private var mContext: Context? = null
    lateinit var mItemsModel: ItemsRootModel
    private var mFilterUiList = ArrayList<FilterRootModel>()
    private var mFilterRequestList = ArrayList<RequestModel>()
    private var dialogFilter: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView =   inflater.inflate(R.layout.fragment_items, container, false)
        mContext = activity

        setToolBarHeading("Items")
        setHasOptionsMenu(true)

        dialogFilter = CommonUtils.dialogInitialize(mContext,R.layout.dialog_item_filter,true,true)

        val mTitleModel = FilterRootModel()
        mTitleModel.data = ArrayList<FilterRootModel>()
        mTitleModel.title = "type"

        for (i in 0 until 2){
            if (i == 0){
                val mChaildModel = FilterRootModel()
                mChaildModel.title = "MATT"
                mChaildModel.id = "MATT"
                mTitleModel.data.add(mChaildModel)
            }else if (i == 1){
                val mChaildModel = FilterRootModel()
                mChaildModel.title = "GLOSSY"
                mChaildModel.id = "GLOSSY"
                mTitleModel.data.add(mChaildModel)
            }
        }
        mFilterUiList.add(mTitleModel)


        getData(Event.item)
        getData(Event.size)

        rootView!!.fab_filter.setOnClickListener(View.OnClickListener {
            dialogFilter!!.show()
        })

        rootView!!.addItem.setOnClickListener(View.OnClickListener {
            var  mBundle =  Bundle()
            mBundle.putString("Type_Add_Edit", "ADD")
            replaceFragment(AddItemFragment(), mBundle, true, true, true, null, R.id.container)
        })

        dialogFilter!!.applyFilterItemBtn.setOnClickListener(View.OnClickListener {
            getData(Event.itemFilter)
            dialogFilter!!.cancel()
            rootView!!.clearBtn.setText(getString(R.string.cleaFilters))
            rootView!!.clearBtn.visibility = View.VISIBLE
        })

        rootView!!.clearBtn.setOnClickListener(View.OnClickListener {
            getData(Event.item)
            rootView!!.clearBtn.visibility = View.GONE
        })

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu);

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
//                CommonUtils.loadToolBarSearch(mContext)
                val mSearchView = CustomeSearchView()
                mSearchView.loadToolBarSearch(mContext, "Search items")
                mSearchView.setOnSearchListener(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    /*Inter face for search Items */
    override fun setSearchListener(status: String?, mSearchString: String) {
        searchString = mSearchString
        getData(Event.itemSearch)
        rootView!!.clearBtn.setText(getString(R.string.clearSearch))
        rootView!!.clearBtn.visibility = View.VISIBLE
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] = PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.item -> {
                RequestManager.addRequest(object : GsonObjectRequest<ItemsRootModel>(
                    WebServices.item, reqHeader, null, ItemsRootModel::class.java, VolleyErrorListener(this, Event.item)
                ) {
                    override fun deliverResponse(response: ItemsRootModel) {
                        updateUi(true, Event.item, response)
                    }
                })
            }

            Event.itemSearch -> {
                RequestManager.addRequest(object : GsonObjectRequest<ItemsRootModel>(
                    WebServices.itemSearch + searchString, reqHeader, null, ItemsRootModel::class.java, VolleyErrorListener(this, Event.item)
                ) {
                    override fun deliverResponse(response: ItemsRootModel) {
                        updateUi(true, Event.item, response)
                    }
                })
            }

            Event.itemFilter -> {
                request = itemFilterRequest()
                logPrint("filter request is: " + request)
                RequestManager.addRequest(object : GsonObjectRequest<ItemsRootModel>(
                    WebServices.itemFilter, reqHeader, request, ItemsRootModel::class.java, VolleyErrorListener(this, Event.item)
                ) {
                    override fun deliverResponse(response: ItemsRootModel) {
                        updateUi(true, Event.item, response)
                    }
                })
            }



            Event.shade -> {
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    WebServices.shade, reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.shade)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.shade, response)
                    }
                })
            }

            Event.kind -> {
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    WebServices.kind, reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.kind)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.kind, response)
                    }
                })
            }

            Event.size -> {
                RequestManager.addRequest(object : GsonObjectRequest<ResponceModel>(
                    WebServices.size, reqHeader, null, ResponceModel::class.java, VolleyErrorListener(this, Event.size)
                ) {
                    override fun deliverResponse(response: ResponceModel) {
                        updateUi(true, Event.size, response)
                    }
                })
            }


        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.item -> {
                    if (serviceResponse is ItemsRootModel){
                   mItemsModel = serviceResponse as ItemsRootModel
                    if (mItemsModel.data != null && mItemsModel.data.size > 0){
                    val mAdapter = ItemsAdapter(mContext, mItemsModel.data)
                    rootView!!.mRecyclerview.adapter = mAdapter
                    mAdapter.setOnCartChangedListener(this)
                    rootView!!.mRecyclerview.visibility =View.VISIBLE

                        return
                }
                }

                    CommonUtils.showToastLong(mContext, "No items found")
                    rootView!!.mRecyclerview.visibility =View.GONE
                }

                Event.size -> {
                    var mSizeModel = serviceResponse as ResponceModel
                    val mTitleModel = FilterRootModel()
                    mTitleModel.data = ArrayList<FilterRootModel>()
                    mTitleModel.title = "size"

                    for (i in 0 until mSizeModel.data.size){
                        val mChaildModel = FilterRootModel()
                        mChaildModel.title = mSizeModel.data[i].code
                        mChaildModel.id = "" + mSizeModel.data[i].id
                        mTitleModel.data.add(mChaildModel)
                    }
                    mFilterUiList.add(mTitleModel)

                    getData(Event.shade)

                }

                Event.shade -> {
                    var mShadeModel = serviceResponse as ResponceModel
                    val mTitleModel = FilterRootModel()
                    mTitleModel.data = ArrayList<FilterRootModel>()
                    mTitleModel.title = "shade"

                    for (i in 0 until mShadeModel.data.size){
                        val mChaildModel = FilterRootModel()
                        mChaildModel.title = mShadeModel.data[i].code
                        mChaildModel.id = "" + mShadeModel.data[i].id
                        mTitleModel.data.add(mChaildModel)
                    }
                    mFilterUiList.add(mTitleModel)

                    getData(Event.kind)

                }

                Event.kind -> {
                    var mKindModel = serviceResponse as ResponceModel
                    val mTitleModel = FilterRootModel()
                    mTitleModel.data = ArrayList<FilterRootModel>()
                    mTitleModel.title = "kind"

                    for (i in 0 until mKindModel.data.size){
                        val mChaildModel = FilterRootModel()
                        mChaildModel.title = mKindModel.data[i].code
                        mChaildModel.id = "" + mKindModel.data[i].id
                        mTitleModel.data.add(mChaildModel)
                    }
                    mFilterUiList.add(mTitleModel)

                    val mAdapter = FilterParentAdapter(mContext, mFilterUiList)
                    dialogFilter!!.filterRecycler.adapter = mAdapter
                    mAdapter.setOnCartChangedListener(this)


                }


            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, "No items found")
            rootView!!.mRecyclerview.visibility =View.GONE
        }
    }

    /*Request for Filter item*/
    private fun itemFilterRequest(): String {
        mFilterRequestList = java.util.ArrayList<RequestModel>()
        for (i in 0 until mFilterUiList.size){
            val mReqModel = RequestModel()
            val valueList = java.util.ArrayList<String>()

            for (j in 0 until mFilterUiList[i].data.size){
                val mModel = mFilterUiList[i].data[j]
                if (mModel.isChecked){
                    mReqModel.key = mFilterUiList[i].title
                    mReqModel.operation = "EQUALS"
                    valueList.add("" + mModel.title)
                }
                mReqModel.valueList = valueList
            }

            if (valueList.size > 0)
                mFilterRequestList.add(mReqModel)

        }

        return Gson().toJson(mFilterRequestList)
    }

    /*Interface for filter items*/
    override fun setCartClickListener(status: String, parentPosition: Int, ChaildPosition: Int, checked: Boolean) {
        mFilterUiList[parentPosition].data[ChaildPosition].isChecked = checked
    }

    /*Interface for items*/
    override fun setCartClickListener(status: String, position: Int) {
        if (status.equals("itemView")){
            var  mBundle =  Bundle()
            mBundle.putString("Id", "" + mItemsModel.data[position].id)
            replaceFragment(ProductDescriptionFragment(), mBundle, true, true, true, null, R.id.container)
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ItemsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ItemsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}
