package com.chs.modulo.ui.activity

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import chs.validateui.Field
import chs.validateui.Form
import chs.validateui.IsEmail
import chs.validateui.NotEmpty
import com.chs.modulo.R
import com.chs.modulo.Utils.AlertDialogHelper
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.RequestModel
import com.chs.modulo.model.ResponceModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : ModuloBaseActivity(), View.OnClickListener, AlertDialogHelper.AlertDialogListener {
    private var mContext: Context? = null
    private lateinit var mSuccessModel: ResponceModel
    private var alertDialog: AlertDialogHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mContext = this

        initViews();
    }

    private fun initViews() {
        alertDialog = AlertDialogHelper(mContext)
        alertDialog!!.setOnAlertListener(this)

        setToolBarHeading("Signup", true)

        signUpBtn.setOnClickListener(this)

    }

    /**
     * actions for the menu item selection
     * @param item
     * @return
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(view: View?) {

        when (view!!.getId()) {
            R.id.signUpBtn -> {
                if (validateUi())
                    getData(Event.RegisterNewCustomer)

            }
        }
    }

    /**
     * Validate the fields
     *
     * @return
     */
    fun validateUi(): Boolean {
        val mForm = Form(mContext as Activity?)
        mForm.addField(Field.using(firstNameEdt).validate(NotEmpty.build(mContext)))
        mForm.addField(Field.using(lastNameEdt).validate(NotEmpty.build(mContext)))

        if (mForm .isValid && (phoneEdt.text!!.length > 10 || phoneEdt.text!!.length < 10)){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                phoneEdt.setError(Html.fromHtml("<font color='red'>" + "Please specify a valid contact number" + "</font>",Html.FROM_HTML_MODE_LEGACY))
            else
                phoneEdt.setError(Html.fromHtml("<font color='red'>" + "Please specify a valid contact number" + "</font>"))

            phoneEdt.requestFocus()
            return false
        }
            mForm.addField(Field.using(emailEdt).validate(NotEmpty.build(mContext)).validate(IsEmail.build(mContext)))
            mForm.addField(Field.using(passwordEdt).validate(NotEmpty.build(mContext)))
            mForm.addField(Field.using(confirmPasswordEdt).validate(NotEmpty.build(mContext)))

            if (mForm .isValid && !passwordEdt.text!!.equals(confirmPasswordEdt.text)){
                CommonUtils.showToast(mContext, "Please check password and Confirm password")
                return false
            }

            return if (mForm.isValid) true else false
        return false
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        var request = ""

        when (actionID) {
            Event.RegisterNewCustomer -> {
                request = RegisterNewCustomerRequest()
                CommonUtils.logPrint(request)
                RequestManager.addRequest(object : GsonObjectRequest<RegisterActivity>(
                    WebServices.RegisterNewCustomer,
                        reqHeader, request, RegisterActivity::class.java, VolleyErrorListener(this, Event.RegisterNewCustomer)
                ) {
                    override fun deliverResponse(response: RegisterActivity) {
                        updateUi(true, Event.RegisterNewCustomer, response)
                    }
                })
            }
        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.RegisterNewCustomer -> {
                    mSuccessModel = serviceResponse as ResponceModel
                    alertDialog!!.showAlertDialog("Youngs Inc", mSuccessModel.statusMessage, "Ok", Event.RegisterNewCustomer, true)

                }

            }
        } else {
            val response = serviceResponse as ErrorModel
            alertDialog!!.showAlertDialog("Register", response.message, "Ok", Event.ExtraDialodParam, true)
        }
    }


    /**
     * Json Request for RegisterNewCustomer
     *
     * @return
     */
    private fun RegisterNewCustomerRequest(): String {
        val mRequestModel = RequestModel()
//        mRequestModel.firstName = buyerNameEdt.text.toString()
//        mRequestModel.company = companyNameEdt.text.toString()
//        mRequestModel.phoneNumber = phoneEdt.text.toString().replace("-", "").replace(" ", "").replace("(", "").replace(")", "")
//        mRequestModel.emailId = emailEdt.text.toString()
//        mRequestModel.salesDevice = getString(R.string.salesDevice)
        return Gson().toJson(mRequestModel)
    }

    /**
     * This method is invoked when the positive button is clicked
     * @param from
     */
    override fun onPositiveClick(from: Int) {
        if (from == Event.RegisterNewCustomer)
            finish()

    }

    /**
     * This method is invoked when the negative button is clicked
     * @param from
     */
    override fun onNegativeClick(from: Int) {

    }

    /**
     * This method is invoked when the neutral button is clicked
     * @param from
     */
    override fun onNeutralClick(from: Int) {

    }

}