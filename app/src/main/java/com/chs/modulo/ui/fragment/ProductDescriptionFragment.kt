package com.chs.modulo.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import chs.ext.GsonObjectRequest
import chs.ext.RequestManager
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.PrefUtil
import com.chs.modulo.adapter.ItemsAdapter
import com.chs.modulo.constants.AppConstants
import com.chs.modulo.constants.ErrorModel
import com.chs.modulo.constants.Event
import com.chs.modulo.constants.VolleyErrorListener
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.ItemObjectRootModel
import com.chs.modulo.model.ItemsRootModel
import com.chs.modulo.model.ResponceModel
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_all_items.view.*
import kotlinx.android.synthetic.main.fragment_design.view.*
import kotlinx.android.synthetic.main.fragment_product_details.*
import kotlinx.android.synthetic.main.fragment_product_details.view.*
import java.util.HashMap

class ProductDescriptionFragment : ModuloBaseFragment() {
    lateinit var mItemsModel: ItemObjectRootModel
    private var rootView: View? = null
    private var mContext: Context? = null
    private var productId: String? = ""

    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        /*Adding Layout to fragment*/
        rootView = inflater.inflate(R.layout.fragment_product_details, null, false)
        mContext = activity
        productId = arguments!!.getString("Id")

        getData(Event.item)

        rootView!!.editBtn.setOnClickListener(View.OnClickListener {
            var mBundle = Bundle()
            mBundle.putString("Type_Add_Edit", "EDIT")
            mBundle.putString("Id", productId)
            replaceFragment(AddItemFragment(), mBundle, true, true, true, null, R.id.container)
        })
        rootView!!.itemStatusBtn.setOnClickListener(View.OnClickListener {
            getData(Event.changeItemStatus)
        })

        return rootView
    }

    /**
     * Requesting api calls
     */
    override fun getData(actionID: Int) {
        showProgressDialog(getString(R.string.please_wait), false)
        val reqHeader = HashMap<String, String>()
        reqHeader["Content-Type"] = "application/json; charset=utf-8"
        reqHeader["Authorization"] =
            PrefUtil.getString(mContext, AppConstants.PREF_Auth_token, AppConstants.PREF_NAME)
        var request = ""

        when (actionID) {
            Event.item -> {
                RequestManager.addRequest(object : GsonObjectRequest<ItemObjectRootModel>(
                    WebServices.item + productId,
                    reqHeader,
                    null,
                    ItemObjectRootModel::class.java,
                    VolleyErrorListener(this, Event.item)
                ) {
                    override fun deliverResponse(response: ItemObjectRootModel) {
                        updateUi(true, Event.item, response)
                    }
                })
            }

            Event.changeItemStatus -> {
                request = changeItemStatusRequest()
                CommonUtils.logPrint("Change status Req is: " + request)
                RequestManager.addRequest(object : GsonObjectRequest<ItemObjectRootModel>(
                    WebServices.item,
                    reqHeader,
                    request,
                    ItemObjectRootModel::class.java,
                    VolleyErrorListener(this, Event.changeItemStatus)
                ) {
                    override fun deliverResponse(response: ItemObjectRootModel) {
                        updateUi(true, Event.changeItemStatus, response)
                    }
                })
            }

        }

    }

    /*Getting responce from Api's and bind to model class*/
    override fun updateUi(status: Boolean, action: Int, serviceResponse: Any) {
        removeProgressDialog()
        if (status) {
            when (action) {
                Event.item -> {
                    mItemsModel = serviceResponse as ItemObjectRootModel

                    setToolBarHeading(mItemsModel.data.label)

                    Picasso.with(mContext)
                        .load(WebServices.imageBaseUrl + mItemsModel.data.label + ".jpg")
                        .placeholder(R.mipmap.ic_launcher)
                        .into(itemImg)

                    productSize.setText(
                        mItemsModel.data.size.code + " | " + mItemsModel.data.size.widthInIn + " * " + mItemsModel.data.size.heightInIn
                                + " | " + mItemsModel.data.size.widthInMm + " * " + mItemsModel.data.size.heightInMm
                    )
                    productShade.setText("" + mItemsModel.data.shade)
                    productKind.setText("" + mItemsModel.data.kind)
                    productDesign.setText("" + mItemsModel.data.design.code)
                    qualities.setText("" + mItemsModel.data.qualities)
                    productDescription.setText("" + mItemsModel.data.description)

                    var itemStatus =
                        if (mItemsModel.data.status == null) "DISABLE" else mItemsModel.data.status

                    itemStatusBtn.setText(if (itemStatus.toUpperCase().equals("ACTIVE")) "DISABLE" else "ACTIVE")
                }

                Event.changeItemStatus ->{
                    mItemsModel = serviceResponse as ItemObjectRootModel
                    var itemSTatus = itemStatusBtn.text.toString()
                    itemStatusBtn.setText(if (itemSTatus.equals("ACTIVE")) "DISABLE" else "ACTIVE")
                    CommonUtils.showToast(mContext, "Item status changed successfully")
                }

            }
        } else {
            val errorResponce = serviceResponse as ErrorModel
            CommonUtils.showToastLong(mContext, errorResponce.message)
        }
    }

    /*Request for login*/
    private fun changeItemStatusRequest(): String {
        mItemsModel.data.status = if (itemStatusBtn.text.toString().equals("ACTIVE")) "ACTIVE" else "DISABLED"
        mItemsModel.data.patch = "patchMethod"

        return Gson().toJson(mItemsModel.data)
//        return Gson().toJson(mItemsModel.data).replace("\"status\":", "\"STATUS\":").replace("\"type\":", "\"TYPE\":")
    }

}