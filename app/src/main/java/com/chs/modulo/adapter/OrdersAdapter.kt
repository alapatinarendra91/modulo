package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils.formateHtml
import com.chs.modulo.model.OrderDTO
import kotlinx.android.synthetic.main.order_child.view.*

class OrdersAdapter (val mContext: Context?, val itemsList: List<OrderDTO>) : RecyclerView.Adapter<OrdersAdapter.ViewHolder>() {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.order_child, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: OrdersAdapter.ViewHolder, position: Int) {
        holder.bindItems(mContext!!, itemsList[position])
        holder.itemView.setOnClickListener {
            onCartChangedListener!!.setCartClickListener("itemView", position)
        }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return itemsList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mContext: Context, mModel: OrderDTO) {

            itemView.orderId.setText(formateHtml("Order Id: " , "" + mModel.id))
            itemView.customerName.setText(formateHtml("Customer: " , "" + mModel.customer.firmName + " " + mModel.customer.lastName))
            itemView.agentName.setText(formateHtml("Agent: " , mModel.userName))
            itemView.companyName.setText(formateHtml("Contact Person: " , mModel.customer.firmName))
            itemView.totalItems.setText(formateHtml("Total Ordered Items: " , "" + mModel.totalItemsCount))
            itemView.status.setText(formateHtml("Status: " , "" + mModel.status))
        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, position: Int)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

}