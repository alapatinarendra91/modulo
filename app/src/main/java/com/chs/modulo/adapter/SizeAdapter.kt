package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.model.SizeMasterDTO
import kotlinx.android.synthetic.main.adapter_designs.view.*

class SizeAdapter (val mContext: Context?, val itemsList: List<SizeMasterDTO>) : RecyclerView.Adapter<SizeAdapter.ViewHolder>() {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SizeAdapter.ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_designs, parent, false)
            return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: SizeAdapter.ViewHolder, position: Int) {
        holder.bindItems(mContext!!, itemsList[position])

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return itemsList.size
//        return 10
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mContext: Context, mModel: SizeMasterDTO) {

                itemView.descriptionTxt.setText(CommonUtils.formateHtml("Size: ",  ("" + mModel.widthInIn + " * "  + mModel.heightInIn
                        + " | " + mModel.widthInMm + " * " +  mModel.heightInMm)))
            itemView.nameTxt.setText(CommonUtils.formateHtml("Status: ", mModel.status.toString()))

        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, position: Int, value: String)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

}