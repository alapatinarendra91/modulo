package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CommonUtils.formateHtml
import com.chs.modulo.interfaces.WebServices
import com.chs.modulo.model.ItemDTO
import com.chs.modulo.model.ResponceModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_all_items.view.*
import kotlinx.android.synthetic.main.adapter_items.view.*

class ItemsAdapter (val mContext: Context?, val itemsList: List<ItemDTO>) : RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_all_items, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ItemsAdapter.ViewHolder, position: Int) {
        holder.bindItems(mContext!!, itemsList[position])
        holder.itemView.setOnClickListener {
            onCartChangedListener!!.setCartClickListener("itemView", position)
        }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return itemsList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mContext: Context, mModel: ItemDTO) {

            Picasso.with(mContext).load(WebServices.imageBaseUrl + mModel.label + ".jpg")
                .placeholder(R.mipmap.ic_launcher)
                .into(itemView.itemImg)

            itemView.itemNumText.setText(formateHtml("" , mModel.kind + " | " + mModel.size.code))
            itemView.descriptionText.setText(formateHtml("" , mModel.label))
        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, position: Int)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

}