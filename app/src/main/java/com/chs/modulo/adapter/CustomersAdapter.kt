package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils.formateHtml
import com.chs.modulo.model.CustomerDTO
import kotlinx.android.synthetic.main.activity_register.view.*
import kotlinx.android.synthetic.main.adapter_cutomers.view.*

class CustomersAdapter (val mContext: Context?, val itemsList: List<CustomerDTO>) : RecyclerView.Adapter<CustomersAdapter.ViewHolder>() {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomersAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_cutomers, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CustomersAdapter.ViewHolder, position: Int) {
        holder.bindItems(mContext!!, itemsList[position])

        holder.itemView.email.setOnClickListener {
            onCartChangedListener!!.setCartClickListener("email", position)
        }
        holder.itemView.mobile.setOnClickListener {
            onCartChangedListener!!.setCartClickListener("mobile", position)
        }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return itemsList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mContext: Context, mModel: CustomerDTO) {

            itemView.companyName.setText(formateHtml("Company Name: " , mModel.firmName))
            itemView.customerName.setText(formateHtml("Name: " , mModel.firstName + " " + mModel.lastName))
            itemView.email.setText(formateHtml("Email: " , mModel.emailId))
            itemView.mobile.setText(formateHtml("Mobile: " , mModel.mobileNumber))
        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, position: Int)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

}