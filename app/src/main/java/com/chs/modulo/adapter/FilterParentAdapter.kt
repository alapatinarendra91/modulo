package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.model.FilterRootModel
import kotlinx.android.synthetic.main.adapter_item_filter_parent.view.*

class FilterParentAdapter (val mContext: Context?, val itemsList: List<FilterRootModel>) : RecyclerView.Adapter<FilterParentAdapter.ViewHolder>(), FilterChaildAdapter.OnCartChangedListener {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterParentAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_item_filter_parent, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: FilterParentAdapter.ViewHolder, position: Int) {
        holder.bindItems(mContext, itemsList[position], position)

        val gridLayoutManager = GridLayoutManager(mContext, 3)
        holder.itemView.filterChaildRecycler.setLayoutManager(gridLayoutManager)

        val mAdapter = FilterChaildAdapter(mContext, itemsList[position].data, position)
        holder.itemView.filterChaildRecycler.adapter = mAdapter
        mAdapter.setOnCartChangedListener(this)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return itemsList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mContext: Context?, mModel: FilterRootModel, parentPosition : Int) {

            itemView.titleHeader.setText(mModel.title)


        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, parentPosition: Int, chaildPosition: Int, checkStatus: Boolean)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun setCartClickListener(status: String, parentPosition: Int, chaildPosition: Int, checkStatus: Boolean) {
        onCartChangedListener!!.setCartClickListener("checkBoxChecked", parentPosition, chaildPosition, checkStatus)
    }

}