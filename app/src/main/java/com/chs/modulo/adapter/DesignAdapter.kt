package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.Utils.CommonUtils.formateHtml
import com.chs.modulo.model.ResponceModel
import kotlinx.android.synthetic.main.adapter_designs.view.*

class DesignAdapter (val mContext: Context?, val itemsList: List<ResponceModel.Data>, val comingFrom: String?) : RecyclerView.Adapter<DesignAdapter.ViewHolder>() {
//class DesignAdapter (val mContext: Context?, val comingFrom: String) : RecyclerView.Adapter<DesignAdapter.ViewHolder>() {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DesignAdapter.ViewHolder {
            /*Design type*/
            val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_designs, parent, false)
            return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: DesignAdapter.ViewHolder, position: Int) {
        holder.bindItems(mContext!!, itemsList[position], comingFrom)

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return itemsList.size
//        return 10
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mContext: Context, mModel: ResponceModel.Data, comingFrom: String?) {
            if (comingFrom.equals(mContext.getString(R.string.design))){
                itemView.nameTxt.setText(formateHtml("" , mModel.status))
                itemView.descriptionTxt.setText(formateHtml("Design Code: " + mModel.code , ""))
            }

            if (comingFrom.equals(mContext.getString(R.string.shades))){
                itemView.descriptionTxt.setText(formateHtml("Shade: " , mModel.code+ " (" + mModel.label + ")"))
                itemView.nameTxt.setText( "" + formateHtml("Status: " , mModel.status))
            }

            if (comingFrom.equals(mContext.getString(R.string.kinds))){
                itemView.descriptionTxt.setText(formateHtml("Code: " , mModel.code ))
                itemView.nameTxt.setText( "" + formateHtml("" , mModel.description) + "\n\n" + mModel.status)
            }

//            Picasso.with(mContext).load(WebServices.imageBaseUrl + mModel.item + ".jpg")
//                .placeholder(R.mipmap.ic_launcher)
//                .into(itemView.itemImage)

        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, position: Int, value: String)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

}