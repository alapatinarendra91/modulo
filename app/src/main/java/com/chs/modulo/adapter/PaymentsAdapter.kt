package com.chs.modulo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chs.modulo.R
import com.chs.modulo.Utils.CommonUtils
import com.chs.modulo.model.PaymentReceiptDTO
import kotlinx.android.synthetic.main.adapter_payments.view.*

class PaymentsAdapter (val mContext: Context?, val itemsList: List<PaymentReceiptDTO>) : RecyclerView.Adapter<PaymentsAdapter.ViewHolder>() {

    private var onCartChangedListener: OnCartChangedListener? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_payments, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: PaymentsAdapter.ViewHolder, position: Int) {
        holder.bindItems(mContext!!, itemsList[position])
        holder.itemView.setOnClickListener {
//            onCartChangedListener!!.setCartClickListener("itemView", position)
        }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return itemsList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(mContext: Context, mModel: PaymentReceiptDTO) {

            itemView.customerNameTxt.setText(CommonUtils.formateHtml("Customer: ", mModel.customer.firmName))
            itemView.agentTxt.setText(CommonUtils.formateHtml("Agent: ", mModel.agent.firstName))
            itemView.amountTxt.setText(CommonUtils.formateHtml("Amount: ", "" + CommonUtils.showTwoDecimals("" + mModel.amount)))
        }

    }


    interface OnCartChangedListener {
        fun setCartClickListener(status: String, position: Int)
    }

    /**
     * Assign the listener implementing events interface that will receive the events
     *
     * @param onCartChangedListener
     */
    fun setOnCartChangedListener(onCartChangedListener: OnCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener
    }

    /* This is for update editetxt changed data*/
    override fun getItemViewType(position: Int): Int {
        return position
    }

}