package com.chs.modulo.interfaces;

/**
 * Created by Calibrage10 on 8/21/2017.
 */

/*
* Declared all API's and called using interface
* */
public interface WebServices {

    // Local URL
//    String BASE_URL = "http://192.168.20.45/YISWarehouseAppService/";

    // Test URL
    String BASE_URL = "http://13.127.221.78:6080/modulo/ora/";

    // Beta URL
//    String BASE_URL = "https://www.youngsinc.com/YIS_APP_CUST_SERVICES_PRO/Beta/";

    /*Live URL*/
//    String BASE_URL = "https://www.youngsinc.com/YIS_APP_CUST_SERVICES_PRO/V4/";


    String imageBaseUrl = "https://res.cloudinary.com/chssoftwares/image/upload/v1582840073/modulo/ora/image/item/";


    String login = BASE_URL + "auth/login";
    String forgot = BASE_URL + "auth/forgot/";
    String item = BASE_URL + "item/";
    String itemSearch = BASE_URL + "item/searchText?keyword=";
    String itemFilter = BASE_URL + "item/search";
    String design = BASE_URL + "design/";
    String shade = BASE_URL + "shade/";
    String kind = BASE_URL + "kind/";
    String size = BASE_URL + "size/";
    String customer = BASE_URL + "customer/";
    String order = BASE_URL + "order/";
    String payment = BASE_URL + "paymentReceipt/";
    String changePassword = BASE_URL + "user/changePassword";
    String changePhoto = BASE_URL + "user/";
    String file = BASE_URL + "file/";
    String address = BASE_URL + "user/";
    String logout = BASE_URL + "logout";
    String adminBulletIn = BASE_URL + "user/adminBulletIn";
    String RegisterNewCustomer = BASE_URL + "RegisterNewCustomer/"; // Sp nsp_app_Scanman_ValidateUser

//    TO address of User
///modulo/ora/user/<userId>/address/


}
