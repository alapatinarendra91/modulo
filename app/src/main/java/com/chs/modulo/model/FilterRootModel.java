package com.chs.modulo.model;

import java.util.ArrayList;

public class FilterRootModel {
    private String title;
    private String id;
    private boolean isChecked;
    private ArrayList<FilterRootModel> data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<FilterRootModel> getData() {
        return data;
    }

    public void setData(ArrayList<FilterRootModel> data) {
        this.data = data;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
