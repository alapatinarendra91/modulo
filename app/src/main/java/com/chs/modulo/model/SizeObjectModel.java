package com.chs.modulo.model;

public class SizeObjectModel {
    private SizeMasterDTO data;

    public SizeMasterDTO getData() {
        return data;
    }

    public void setData(SizeMasterDTO data) {
        this.data = data;
    }
}
