package com.chs.modulo.model;

public class ItemObjectRootModel {
    private ItemDTO data;
    private UserDTO userDTO;
    private AddressDTO addressDTO;
    private String token;

    public ItemDTO getData() {
        return data;
    }

    public void setData(ItemDTO data) {
        this.data = data;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AddressDTO getAddressDTO() {
        return addressDTO;
    }

    public void setAddressDTO(AddressDTO addressDTO) {
        this.addressDTO = addressDTO;
    }
}
