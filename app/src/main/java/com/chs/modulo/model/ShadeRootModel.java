package com.chs.modulo.model;

import java.util.ArrayList;

public class ShadeRootModel {
    private ArrayList<ShadeMasterDTO> data;

    public ArrayList<ShadeMasterDTO> getData() {
        return data;
    }

    public void setData(ArrayList<ShadeMasterDTO> data) {
        this.data = data;
    }
}
