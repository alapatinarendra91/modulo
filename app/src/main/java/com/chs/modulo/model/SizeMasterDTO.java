/**
 * 
 */
package com.chs.modulo.model;

import com.chs.modulo.constants.Status;

public class SizeMasterDTO {
	
	private Integer id;
	private String code;
	private int  widthInMm;
	private int  heightInMm;
	
	private int  widthInIn;
	private int  heightInIn;
	
	private String createdBy;
	private String createdOn;
	private String modifiedBy;
	private String modifiedOn;
	
//	@Enumerated(EnumType.STRING)
	private Status status ;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getWidthInMm() {
		return widthInMm;
	}

	public void setWidthInMm(int widthInMm) {
		this.widthInMm = widthInMm;
	}

	public int getHeightInMm() {
		return heightInMm;
	}

	public void setHeightInMm(int heightInMm) {
		this.heightInMm = heightInMm;
	}

	public int getWidthInIn() {
		return widthInIn;
	}

	public void setWidthInIn(int widthInIn) {
		this.widthInIn = widthInIn;
	}

	public int getHeightInIn() {
		return heightInIn;
	}

	public void setHeightInIn(int heightInIn) {
		this.heightInIn = heightInIn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
	
	
}
