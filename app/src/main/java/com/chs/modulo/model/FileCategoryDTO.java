/**
 * 
 */
package com.chs.modulo.model;

import java.util.List;

/**
 * @author Narendra
 *
 */
public class FileCategoryDTO {

	private Integer id;
	private String type;//
	private String relatedCodes;
	private List<String> codesList;
//	private byte[] icon;
	
	private String createdBy;
	private String createdOn;
	private String modifiedBy;
	private String modifiedOn;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRelatedCodes() {
		return relatedCodes;
	}
	public void setRelatedCodes(String relatedCodes) {
		this.relatedCodes = relatedCodes;
	}
	public List<String> getCodesList() {
		return codesList;
	}
	public void setCodesList(List<String> codesList) {
		this.codesList = codesList;
	}
//	public byte[] getIcon() {
//		return icon;
//	}
//	public void setIcon(byte[] icon) {
//		this.icon = icon;
//	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
}
