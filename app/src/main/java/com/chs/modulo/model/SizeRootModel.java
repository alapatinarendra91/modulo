package com.chs.modulo.model;

import java.util.ArrayList;

public class SizeRootModel {
    private ArrayList<SizeMasterDTO> data;

    public ArrayList<SizeMasterDTO> getData() {
        return data;
    }

    public void setData(ArrayList<SizeMasterDTO> data) {
        this.data = data;
    }
}
