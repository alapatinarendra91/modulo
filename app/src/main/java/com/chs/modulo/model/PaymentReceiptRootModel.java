package com.chs.modulo.model;

import java.util.ArrayList;

public class PaymentReceiptRootModel {
    private ArrayList<PaymentReceiptDTO> data;

    public ArrayList<PaymentReceiptDTO> getData() {
        return data;
    }

    public void setData(ArrayList<PaymentReceiptDTO> data) {
        this.data = data;
    }
}
