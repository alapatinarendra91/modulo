/**
 * 
 */
package com.chs.modulo.model;

/**
 * @author Narendra
 *
 */
public class FileDTO {

	private Integer id;
	private FileCategoryDTO category;
	private String fileName;
	private Long sizeInBytes;
	private String content;
	private String contentType;
	private String createdBy;
	private String createdOn;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public FileCategoryDTO getCategory() {
		return category;
	}

	public void setCategory(FileCategoryDTO category) {
		this.category = category;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getSizeInBytes() {
		return sizeInBytes;
	}
	public void setSizeInBytes(Long sizeInBytes) {
		this.sizeInBytes = sizeInBytes;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "FileDTO [id=" + id + ", mimeType=" + category + ", fileName=" + fileName
				+ ", sizeInBytes=" + sizeInBytes+ "]";
	}
}
