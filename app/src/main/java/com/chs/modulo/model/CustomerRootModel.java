package com.chs.modulo.model;

import java.util.ArrayList;

public class CustomerRootModel {
    private ArrayList<CustomerDTO> data;

    public ArrayList<CustomerDTO> getData() {
        return data;
    }

    public void setData(ArrayList<CustomerDTO> data) {
        this.data = data;
    }
}
