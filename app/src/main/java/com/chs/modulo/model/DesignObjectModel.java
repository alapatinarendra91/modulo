package com.chs.modulo.model;

public class DesignObjectModel {
    private DesignMasterDTO data;

    public DesignMasterDTO getData() {
        return data;
    }

    public void setData(DesignMasterDTO data) {
        this.data = data;
    }
}
