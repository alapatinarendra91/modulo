/**
 * 
 */
package com.chs.modulo.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sridhar
 *
 */
public class OrderDTO implements Comparable<OrderDTO> {

	private Integer id;	
	private String userName;
	private String initiatedOn;
	private int totalItemsCount;
	private String lastModifiedOn;
	private String lastSubmittedOn;
	private Float latitude;
	private Float longitude;
	private CustomerDTO customer;
	private UserDTO user;
	private String comments;
	private Boolean eligibleForReEdit;
	private String status ;
	private String brand;
	private List<OrderItemDTO> orderItems = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getInitiatedOn() {
		return initiatedOn;
	}
	public void setInitiatedOn(String initiatedOn) {
		this.initiatedOn = initiatedOn;
	}
	public int getTotalItemsCount() {
		if(this.totalItemsCount == 0 && this.getOrderItems().size() > 0) {
			for (OrderItemDTO item : this.getOrderItems()) {
				this.totalItemsCount = this.totalItemsCount +item.getQuantity();
			}
		}
		return totalItemsCount;
	}
	public void setTotalItemsCount(int totalItemsCount) {
		this.totalItemsCount = totalItemsCount;
	}
	public String getLastModifiedOn() {
		return lastModifiedOn;
	}
	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	public String getLastSubmittedOn() {
		return lastSubmittedOn;
	}
	public void setLastSubmittedOn(String lastSubmittedOn) {
		this.lastSubmittedOn = lastSubmittedOn;
	}
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	public Float getLongitude() {
		return longitude;
	}
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}
	public CustomerDTO getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<OrderItemDTO> getOrderItems() {
		return orderItems;
	}
	public Boolean isEligibleForReEdit() {
		return eligibleForReEdit;
	}
	public void setEligibleForReEdit(Boolean eligibleForReEdit) {
		this.eligibleForReEdit = eligibleForReEdit;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public void setOrderItems(List<OrderItemDTO> orderItems) {
		this.orderItems = orderItems;
	}

	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Boolean getEligibleForReEdit() {
		return eligibleForReEdit;
	}
	@Override
	public int compareTo(OrderDTO compareOrder) {
		int compareId = compareOrder.getId();
		return this.getId() - compareId;
	}
}
