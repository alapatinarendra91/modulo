package com.chs.modulo.model;

import java.util.ArrayList;

public class DesignRootModel {
    private ArrayList<DesignMasterDTO> data;

    public ArrayList<DesignMasterDTO> getData() {
        return data;
    }

    public void setData(ArrayList<DesignMasterDTO> data) {
        this.data = data;
    }
}
