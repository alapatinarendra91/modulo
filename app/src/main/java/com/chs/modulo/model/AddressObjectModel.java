package com.chs.modulo.model;

public class AddressObjectModel {
    private AddressDTO data;

    public AddressDTO getData() {
        return data;
    }

    public void setData(AddressDTO data) {
        this.data = data;
    }
}
