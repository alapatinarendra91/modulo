package com.chs.modulo.model;

import java.util.List;

public class ItemsRootModel {
    private List<ItemDTO> data;

    public List<ItemDTO> getData() {
        return data;
    }

    public void setData(List<ItemDTO> data) {
        this.data = data;
    }
}
