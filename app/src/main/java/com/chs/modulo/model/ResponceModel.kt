package com.chs.modulo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponceModel(
    val `data`: List<Data>,
    val `data2`: List<Data>,
    val statusCode: Int,
    val statusMessage: String
) {
    class Data : Serializable {
        @SerializedName("description")
        @Expose
        var description: String = ""
        @SerializedName("status")
        @Expose
        var status: String = ""
        @SerializedName("type")
        @Expose
        var type: String = ""
        @SerializedName("label")
        @Expose
        var label: String = ""
        @SerializedName("id")
        @Expose
        var id: String = ""
        @SerializedName("shade")
        @Expose
        var shade: String = ""
        @SerializedName("kind")
        @Expose
        var kind: String = ""
        @SerializedName("code")
        @Expose
        var code: String = ""
        @SerializedName("firmName")
        @Expose
        var firmName: String = ""
        @SerializedName("userName")
        @Expose
        var userName: String = ""
        @SerializedName("totalItemsCount")
        @Expose
        var totalItemsCount: String = ""

//        @SerializedName("size")
//        @Expose
//        val `size`: ResponceModel.Data
//            get() {
//                TODO()
//            }

    }
}