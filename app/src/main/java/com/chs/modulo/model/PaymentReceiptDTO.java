/**
 * 
 */
package com.chs.modulo.model;

/**
 * @author sridhar
 *
 */
public class PaymentReceiptDTO implements Comparable<PaymentReceiptDTO>{

	private Integer id;
	private CustomerDTO customer;
	private Float amount;
	private FileDTO receipt;
	private UserDTO agent;
	private String createdBy;
	private String createdOn;
	private String modifiedBy;
	private String modifiedOn;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public CustomerDTO getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public FileDTO getReceipt() {
		return receipt;
	}
	public void setReceipt(FileDTO receipt) {
		this.receipt = receipt;
	}
	public UserDTO getAgent() {
		return agent;
	}
	public void setAgent(UserDTO agent) {
		this.agent = agent;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Override
	public int compareTo(PaymentReceiptDTO compareReceipt) {
		int compareId = compareReceipt.getId();
		return this.getId() - compareId;
	}
}
