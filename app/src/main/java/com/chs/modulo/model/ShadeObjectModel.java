package com.chs.modulo.model;

public class ShadeObjectModel {
    private ShadeMasterDTO data;

    public ShadeMasterDTO getData() {
        return data;
    }

    public void setData(ShadeMasterDTO data) {
        this.data = data;
    }
}
