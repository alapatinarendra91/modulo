/**
 * 
 */
package com.chs.modulo.model;

/**
 * @author sridhar
 *
 */
public class ShadeMasterDTO {

	private Integer id;
	private String code;
	private String description;
	private String label;
	
	private String createdBy;
	private String createdOn;
	private String modifiedBy;
	private String modifiedOn;
	
	private String status;
	
	public ShadeMasterDTO() {
		// TODO Auto-generated constructor stub
	}
	
	

	public ShadeMasterDTO(Integer id, String code, String description, String label, String createdBy,
						  String createdOn, String modifiedBy, String modifiedOn, String status) {
		super();
		this.id = id;
		this.code = code;
		this.description = description;
		this.label = label;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShadeMasterDTO other = (ShadeMasterDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "ShadeMasterDTO [id=" + id + ", code=" + code + ", description=" + description + ", label=" + label
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + ", status=" + status + "]";
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	
	public String getCreatedOn() {
		return createdOn;
	}



	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}



	public String getModifiedOn() {
		return modifiedOn;
	}



	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}



	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
