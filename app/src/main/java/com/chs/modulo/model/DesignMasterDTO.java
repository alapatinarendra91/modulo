/**
 * 
 */
package com.chs.modulo.model;


import com.chs.modulo.constants.DesignOwnerType;
import com.chs.modulo.constants.Status;

/**
 * @author sridhar
 *
 */
public class DesignMasterDTO {

	private Integer id;
	private String code;
	private String description;
	
	private String createdBy;
	private String createdOn;
	private String modifiedBy;
	private String modifiedOn;
	
	private DesignOwnerType designedBy = DesignOwnerType.COMPANY;

	private String status;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public DesignOwnerType getDesignedBy() {
		return designedBy;
	}
	public void setDesignedBy(DesignOwnerType designedBy) {
		this.designedBy = designedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
