package com.chs.modulo.model;

import java.util.ArrayList;

public class OrdersRootModel {
    private ArrayList<OrderDTO> data;

    public ArrayList<OrderDTO> getData() {
        return data;
    }

    public void setData(ArrayList<OrderDTO> data) {
        this.data = data;
    }
}
