/**
 * 
 */
package com.chs.modulo.model;

public class ItemDTO {

	private Integer id;
	private String label;
	private DesignMasterDTO design;
//	@Enumerated(EnumType.STRING)
//	private ItemType type;
	private String type;
	private String shade;
	private String kind;
	private SizeMasterDTO size;
	private String description;
	private String qualities;
	private String createdBy;
	private String createdOn;
	private String modifiedBy;
	private String modifiedOn;
//	@Enumerated(EnumType.STRING)
	private String status;
//	@Enumerated(EnumType.STRING)
	private String brand;
	private String patch;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public DesignMasterDTO getDesign() {
		return design;
	}
	public void setDesign(DesignMasterDTO design) {
		this.design = design;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getShade() {
		return shade;
	}
	public void setShade(String shade) {
		this.shade = shade;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public SizeMasterDTO getSize() {
		return size;
	}
	public void setSize(SizeMasterDTO size) {
		this.size = size;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getQualities() {
		return qualities;
	}
	public void setQualities(String qualities) {
		this.qualities = qualities;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPatch() {
		return patch;
	}

	public void setPatch(String patch) {
		this.patch = patch;
	}
}
