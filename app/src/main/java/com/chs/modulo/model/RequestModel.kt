package com.chs.modulo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class RequestModel {
    @SerializedName("userName")
    @Expose
    var userName: String? = ""
    @SerializedName("password")
    @Expose
    var password: String? = ""
    @SerializedName("userId")
    @Expose
    var userId: String? = ""
    @SerializedName("oldPassword")
    @Expose
    var oldPassword: String? = ""
    @SerializedName("newPassword")
    @Expose
    var newPassword: String? = ""
    @SerializedName("patch")
    @Expose
    var patch: String? = ""
    @SerializedName("key")
    @Expose
    var key: String? = ""
    @SerializedName("operation")
    @Expose
    var operation: String? = ""
    @SerializedName("valueList")
    @Expose
    var valueList: ArrayList<String> = ArrayList()

}