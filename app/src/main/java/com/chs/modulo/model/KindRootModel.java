package com.chs.modulo.model;

import java.util.ArrayList;

public class KindRootModel {
    private ArrayList<KindMasterDTO> data;

    public ArrayList<KindMasterDTO> getData() {
        return data;
    }

    public void setData(ArrayList<KindMasterDTO> data) {
        this.data = data;
    }
}
