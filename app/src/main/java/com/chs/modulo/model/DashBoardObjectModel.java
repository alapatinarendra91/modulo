package com.chs.modulo.model;

public class DashBoardObjectModel {
    private DashBoardObjectModel data;
    private String toalOrders;
    private String activeOrders;
    private String totalCustomers;
    private String activeCustomers;
    private String paymentReceiptsAll;
    private String paymentReceipts12Months;
    private String totalAgents;
    private String activeAgents;

    public DashBoardObjectModel getData() {
        return data;
    }

    public void setData(DashBoardObjectModel data) {
        this.data = data;
    }

    public String getToalOrders() {
        return toalOrders;
    }

    public void setToalOrders(String toalOrders) {
        this.toalOrders = toalOrders;
    }

    public String getActiveOrders() {
        return activeOrders;
    }

    public void setActiveOrders(String activeOrders) {
        this.activeOrders = activeOrders;
    }

    public String getTotalCustomers() {
        return totalCustomers;
    }

    public void setTotalCustomers(String totalCustomers) {
        this.totalCustomers = totalCustomers;
    }

    public String getActiveCustomers() {
        return activeCustomers;
    }

    public void setActiveCustomers(String activeCustomers) {
        this.activeCustomers = activeCustomers;
    }

    public String getPaymentReceiptsAll() {
        return paymentReceiptsAll;
    }

    public void setPaymentReceiptsAll(String paymentReceiptsAll) {
        this.paymentReceiptsAll = paymentReceiptsAll;
    }

    public String getPaymentReceipts12Months() {
        return paymentReceipts12Months;
    }

    public void setPaymentReceipts12Months(String paymentReceipts12Months) {
        this.paymentReceipts12Months = paymentReceipts12Months;
    }

    public String getTotalAgents() {
        return totalAgents;
    }

    public void setTotalAgents(String totalAgents) {
        this.totalAgents = totalAgents;
    }

    public String getActiveAgents() {
        return activeAgents;
    }

    public void setActiveAgents(String activeAgents) {
        this.activeAgents = activeAgents;
    }
}
