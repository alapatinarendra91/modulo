package com.chs.modulo.model;

public class KindObjectModel {
    private KindMasterDTO data;

    public KindMasterDTO getData() {
        return data;
    }

    public void setData(KindMasterDTO data) {
        this.data = data;
    }
}
