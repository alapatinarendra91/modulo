package com.chs.modulo.constants;

/**
 * Created by ANDROID on 6/10/2016.
 */
public interface Event {
    /**
     * int constants for framework dialogs/events/API calls
     */

    int PERMISSION_REQUEST_Location = 1;
    int PERMISSION_REQUEST_File = 2;
    int PERMISSION_REQUEST_PhoneCall = 3;
    int PERMISSION_REQUEST_Camera = 4;
    int login = 5;
    int forgot = 6;
    int ExtraDialodParam = 7;
    int RegisterNewCustomer = 8;
    int design = 9;
    int item = 10;
    int customer = 11;
    int order = 12;
    int itemSearch = 13;
    int shade = 14;
    int kind = 15;
    int size = 16;
    int payment = 17;
    int changePassword = 18;
    int getAddress = 19;
    int address = 20;
    int itemFilter = 21;
    int logout = 22;
    int file = 23;
    int changePhoto = 24;
    int changeItemStatus = 25;
    int updateItem = 26;
    int adminBulletIn = 27;

}
