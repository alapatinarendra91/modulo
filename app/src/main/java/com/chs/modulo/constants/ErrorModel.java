package com.chs.modulo.constants;


import chs.model.BaseModel;

/**
 * Created by Narendra on 15/06/15.
 */
/*Getter and setter methods are used to retrieve and manipulate private variables in a different class.*/
public class ErrorModel extends BaseModel {
    int timestamp;
    int statusCode;
    String error;
    String error_description;
    String data;
    String message;

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public String getdata() {
        return data;
    }

    public void setdata(String data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
