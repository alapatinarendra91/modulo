/**
 * 
 */
package com.chs.modulo.constants;

/**
 * @author sridhar
 *
 */
public enum DesignOwnerType {
	COMPANY("COMPANY"),CUSTOMER("CUSTOMER");
	
	private String code;
	public String getCode() {
		return this.code;
	}
	
	private DesignOwnerType(String code) {
		this.code = code;
	}
}
