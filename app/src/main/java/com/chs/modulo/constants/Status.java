/**
 * 
 */
package com.chs.modulo.constants;

/**
 * @author sridhar
 *
 */
public enum Status {
	ACTIVE("ACTIVE"), DISABLED("DISABLED");
	
	private String code;
	
	private Status(String code) {
		this.code = code;
	}
	public String getCode() {
		return this.code;
	}
}
