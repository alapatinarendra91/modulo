package com.chs.modulo.constants;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.chs.modulo.Utils.CommonUtils;
import com.warehouse.ui.activity.LoginActivity;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import chs.ui.IScreen;

/***
 * @author Narendra
 * API Error handle for all API's
 */

public class VolleyErrorListener implements Response.ErrorListener {
    private final int action;
    private final IScreen screen;
    private ErrorModel errorModel_setdata;

    public VolleyErrorListener(final IScreen screen, final int action) {
        this.screen = screen;
        this.action = action;

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String str = "";
        int code = 0;
        ErrorModel errorModel = new ErrorModel();

        if (error != null && error.networkResponse != null && error.networkResponse.data != null) {

            try {
                str = new String(error.networkResponse.data, "UTF-8");
                code = error.networkResponse.statusCode;
                CommonUtils.logPrint("Code", "" + code);
                CommonUtils.logPrint("str", "" + str);
                errorModel = jsonToArray(str, code);
                if (errorModel != null) {
                    if (code == 401){
                        try {
                            errorModel.setMessage("Authorization has been denied for this request. Please Login again...");
                            CommonUtils.mContext.startActivity(new Intent(CommonUtils.mContext, LoginActivity.class));
                        }catch (Exception e){

                        }
                    }
                    screen.updateUi(false, action, errorModel);
                } else {
                    errorModel = new ErrorModel();

                  if (code >= 402 && code < 500) {
                        errorModel.setMessage("Bad Request");
                        screen.updateUi(false, action, errorModel);
                    } else if (code >= 500){
                        errorModel.setMessage("Server error");
                        screen.updateUi(false, action, errorModel);
                    }else {
//                      errorModel.setMessage("Unknown error");
//                      screen.updateUi(false, action, errorModel);
                  }
                }
                return;
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        } else if (screen != null)
            if (error instanceof NoConnectionError) {
                CommonUtils.logPrint("VolleyError", "here1");
                try {
                    CommonUtils.logPrint("Response", "" + error);
                    str = new String(error.networkResponse.data, "UTF-8");
                    CommonUtils.logPrint("VolleyError", str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                errorModel.setMessage(Constant.NoConnectionError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof AuthFailureError) {
                errorModel.setMessage(Constant.AuthFailureError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof NetworkError) {
                errorModel.setMessage(Constant.NetworkError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof ParseError) {
                errorModel.setMessage(Constant.ParseError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof ServerError) {
                errorModel.setMessage(Constant.ServerError);
                screen.updateUi(false, action, errorModel);
            } else if (error instanceof TimeoutError) {
                errorModel.setMessage(Constant.TimeoutError);
                screen.updateUi(false, action, errorModel);
            }
    }

    /***
     *
     * @param jsonInput
     * @param statusCode
     * @return error_description
     */
    private ErrorModel jsonToArray(String jsonInput, int statusCode) {
        try {
            errorModel_setdata = new ErrorModel();

            if (jsonInput.trim().length() > 0) {
                JSONObject jsonObject = new JSONObject(jsonInput);

                if (jsonObject.has("timestamp"))
                    errorModel_setdata.setTimestamp(jsonObject.optInt("timestamp"));
                if (jsonObject.has("statusCode"))
                    errorModel_setdata.setStatusCode(jsonObject.optInt("statusCode"));
                if (jsonObject.has("error"))
                    errorModel_setdata.setError(jsonObject.optString("error"));
                if (jsonObject.has("error_description"))
                    errorModel_setdata.setError_description(jsonObject.optString("error_description"));
                if (jsonObject.has("message"))
                    errorModel_setdata.setMessage(jsonObject.optString("message"));
            }
            return  errorModel_setdata;
        } catch (Exception e) {
            CommonUtils.logPrint("","exception is : "+e.toString());
            return  errorModel_setdata;
        }
    }
}
