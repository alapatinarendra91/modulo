package com.warehouse.constants

  class Event_kotlin {
    var PERMISSION_REQUEST_Location: Int = 1
    var PERMISSION_REQUEST_File = 2
    var PERMISSION_REQUEST_PhoneCall = 3
    var PERMISSION_REQUEST_Camera = 4
}