package com.chs.modulo.constants;

/**
 * constants to be used store values
 *
 * @author Narendra
 */

public class AppConstants {
    /*Key values used in SharedPreferences*/
    public static String gcmRegId = "gcmRegId";
    public static String PREF_NAME = "Modulo_pref";
    public static String PREF_Auth_token = "auth_token";
    public static String PREF_UserName = "PREF_UserName";
    public static String PREF_MobileNo = "PREF_MobileNo";
    public static String PREF_DisplayName = "PREF_DisplayName";
    public static String PREF_UserID = "PREF_UserID";
    public static String PREF_Name = "PREF_Name";
    public static String PREF_Email = "PREF_Email";
    public static String PREF_Password = "PREF_Password";
    public static String PREF_Address_Id = "PREF_Address_Id";
    public static String PREF_RememberMe = "PREF_RememberMe";


    public static String queuesToPullDateFormate = "MM/dd/yy";
    public static String userDateFormate = "MM/dd/yyyy";
    public static String serverDateFormate = "yyyy-MM-dd";


    /*Used For intents only*/
    public static String Intent_resetPasswordReqd = "Intent_resetPasswordReqd";
    public static String Intent_ComingFrom = "Intent_ComingFrom";
}
