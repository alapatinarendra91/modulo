/**
 * 
 */
package com.chs.modulo.constants;

/**
 * @author sridhar
 *
 */
public enum BrandType {
	MODULO("MODULO"),QUADRO("QUADRO");
	private String code;
	
	public String getCode() {
		return this.code;
	}
	private  BrandType(String code) {
		this.code = code;
	}
}
